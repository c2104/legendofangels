$(function() {
    $('#header-toggle__dev').on('click', function() {
        var dev_status;
        if($('.header-dropdown__content--toggle-dev').prop('checked')) {
            dev_status = 0;
        } else {
            dev_status = 1;
        }

        $.ajax({
            url      : '/admin/api/function/change_developer',
            type     : 'POST',
            dataType : 'json',
            cache    : false,
            data     : {
                member_id  : $(this).attr('data-member-id'),
                dev_status : dev_status
            }
        })
        .done(function(res) {
            document.location.reload(true);
        });
    });
});