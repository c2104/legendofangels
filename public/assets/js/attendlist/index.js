$(function() {
    // テーブルスクロール
    tableScroll();

    // テーブル列行のhover用tableオブジェクト
    $('.table-scroll').on("mouseenter mouseleave", "td", tableColor);

    $('.js-attendance').each(function() {
        var attendance = $(this).text().trim()
        if (attendance == '未定') {
            $(this).addClass('undecided');
        }
    });
});

// テーブルスクロール
function tableScroll() {
    function $e(id) {
        return document.getElementById(id);
    }
    function scroll() {
        $e("header-h").scrollLeft= $e("data").scrollLeft;// 左右連動させる
        $e("header-v").scrollTop = $e("data").scrollTop;// 上下連動させる
    }
    $e("data").onscroll=scroll;
}