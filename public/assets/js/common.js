// テーブルのcellにhoverで背景をつける
function tableColor(event) {
    var col_idx = $(this).index() + 1;
    var row_idx = $(this).closest("tr")[0].rowIndex + 1;

    tds = $(this).closest("table").find("td:nth-child(" + col_idx + ")");
    hds = $(this).closest("div").prev().prev().children("table").find("td:nth-child(" + col_idx + ")");
    vds = $(this).closest("div").prev().children("table").find("tr:nth-child(" + row_idx + ")");

    if ($(this).closest("div").attr('id') == 'data') {
        if (event.type === "mouseenter") {
            tds.addClass("tds_over");
            hds.addClass("over_active");
            vds.addClass("over_active");
            $(this).addClass("over_active");
            $(this).siblings().addClass("over_active");
        } else {
            tds.removeClass("tds_over");
            hds.removeClass("over_active");
            vds.removeClass("over_active");
            $(this).removeClass("over_active");
            $(this).siblings().removeClass("over_active");
        }
    }
}