$(function () {

    // 出欠
    (function() {
        var new_status_obj    = $('.modal-chg-attend__body--new');
        var alert_obj         = $('.modal-chg-attend__body--alert');
        var change_submit_btn = $('.modal-chg-attend__footer--change');

        // 登録
        submitAttendance();

        // 変更
        changeAttendance();

        // 出欠登録
        function submitAttendance() {
            $('#attendance').submit(function() {
                var res = confirm('出欠を登録します。');
                if (!res) {
                    return false;
                }
            });
        }

        $('.attendance-table__attendance').each(function() {
            var attendance = $(this).text().trim()
            if (attendance == '未定') {
                $(this).parent().addClass('undecided');
            }
        });

        // 出欠変更
        function changeAttendance() {
            var data = {};

            // モーダルを初期化
            $('.attendance-table__change').on('click', function() {
                initModalChangeAttendance(this);

                data['hash']        = $(this).attr('data-hash');
                data['schedule_id'] = $(this).attr('data-schedule-id');
                data['attend_id']   = $(this).attr('data-attend-id');

                new_status_obj.children('[value='+data['attend_id']+']').hide();
            });

            // 変更処理
            change_submit_btn.on('click', function() {
                if ($('#form_new-attendance').val() == 0 || $('#form_new-attendance').val() == '') {
                    alert_obj.addClass('alert-error').removeClass('alert-info').text('変更する出欠を選択してください').show();
                    return false;
                } else if ($('#form_new-attendance').val() == data['attend_id']) {
                    alert_obj.addClass('alert-error').removeClass('alert-info').text('変更前と同じ出欠が選択されています').show();
                    return false;
                } else {
                    alert_obj.hide();
                }

                // 出欠セレクトボックスと変更ボタンをdisabled
                new_status_obj.attr('disabled', 'disabled');
                $(this).attr('disabled', 'disabled');

                // 新しい出欠を取得
                data['new_attend_id'] = $('#form_new-attendance').val();

                $.ajax({
                    url      : $(this).attr('href'),
                    type     : 'POST',
                    dataType : 'json',
                    data     : data,
                })
                .done(function(res) {
                    // 変更ボタンを隠す
                    change_submit_btn.hide();

                    // スケジュールの表記を変更
                    var attend_text = $('#form_new-attendance option:selected').text();
                    var list_change_btn = $('.attendance-table__change[data-schedule-id='+data['schedule_id']+']');
                    list_change_btn.siblings('.attendance-table__attendance').text(attend_text);
                    list_change_btn.attr('data-attend-id', data['new_attend_id']);

                    // 未定の場合の色を戻す
                    if (attend_text == '未定') {
                        list_change_btn.parent().addClass('undecided');
                    } else {
                        list_change_btn.parent().removeClass('undecided');
                    }

                    // 完了メッセージ
                    alert_obj.addClass('alert-info').removeClass('alert-error').text(res).show();
                })
                .fail(function() {
                    alert_obj.addClass('alert-error').removeClass('alert-info').text('変更できませんでした。').show();
                });
            });
        }

        function initModalChangeAttendance(change_btn) {
            new_status_obj.removeAttr('disabled');
            new_status_obj.val('');
            new_status_obj.children().show();
            alert_obj.hide();
            change_submit_btn.show().removeAttr('disabled');

            var date   = $(change_btn).siblings('.attendance-table__date').text();
            var dow    = $(change_btn).siblings('.attendance-table__dow').html();
            var attend = $(change_btn).siblings('.attendance-table__attendance').text();
            $('.modal-chg-attend__body--date').html(date+dow);
            $('.modal-chg-attend__body--old').html(attend);
        }
    }());

    // attendance-headを固定
    (function(){
        fixedAttendanceHeader();

        // attendance-headを固定
        function fixedAttendanceHeader() {
            var scroll_content = $('.attendance-body');
            var fixed_content  = $('.attendance-head');
            var offset         = fixed_content.offset();

            $('.navbar-toggle').on('click', function() {
                setTimeout(function insert() {
                    scroll_content = $('.attendance-body');
                    fixed_content  = $('.attendance-head');
                    offset         = fixed_content.offset();
                });
            });

            $(window).scroll(function() {
                if ($(window).scrollTop() > offset.top) {
                    fixed_content.addClass('fixed');
                    scroll_content.addClass('scrolled');
                } else {
                    fixed_content.removeClass('fixed');
                    scroll_content.removeClass('scrolled');
                }
            });
        }
    }());
});
