<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.1
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2018 Fuel Development Team
 * @link       http://fuelphp.com
 */

// Bootstrap the framework DO NOT edit this
require COREPATH.'bootstrap.php';

$add_classes = [
    'Mobile_Detect' => APPPATH . 'vendor/mobile_detect.php',
    'Commonfunc'    => APPPATH . 'classes/lib/commonfunc.php',
    'JoinYear'      => APPPATH . 'classes/lib/joinyear.php',

    'Fuel'          => APPPATH . 'classes/fuel.php',
    'Log'           => APPPATH . 'classes/log.php',

    // 基底クラス
    'Model_La'   => APPPATH.'bases/model/la.php',
    'Service_La' => APPPATH.'bases/service/la.php',
];

\Autoloader::add_classes($add_classes);

\Autoloader::add_classes(getAutoloadableClassMap(APPPATH.'classes/lib'));

// Register the autoloader
\Autoloader::register();

/**
 * Your environment.  Can be set to any of the following:
 *
 * Fuel::DEVELOPMENT
 * Fuel::TEST
 * Fuel::STAGING
 * Fuel::PRODUCTION
 */
\Fuel::$env = \Arr::get($_SERVER, 'FUEL_ENV', \Arr::get($_ENV, 'FUEL_ENV', getenv('FUEL_ENV') ?: \Fuel::DEVELOPMENT));

// Initialize the framework with the config file.
\Fuel::init('config.php');



/**
 * 指定したディレクトリの下のクラス名を Autoloader::add_classes() に渡せる配列として取得
 *
 * @param $directory
 * @return array
 */
function getAutoloadableClassMap($directory)
{
    $filenames = array_filter(scandir($directory), function ($filename) {
        return !in_array($filename, ['.', '..'], true);
    });
    $map = [];
    foreach ($filenames as $filename) {
        $filepath = rtrim($directory, '/') . '/' . $filename;

        if (is_file($filepath)) {
            $snakecase_name = rtrim($filename, '.php');
            $map = array_merge($map, [str_replace('_', '', ucwords($snakecase_name, '_')) => $filepath]);
        }
        if (is_dir($filepath)) {
            $map = array_merge($map, getAutoloadableClassMap($filepath));
        }
    }

    return $map;
}
