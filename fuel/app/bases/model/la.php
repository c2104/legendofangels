<?php

/**
 * coreのModelを拡張
 *
 */
class Model_La extends Fuel\Core\Model
{
    protected static $table = null;
    protected static $primary_key = 'id';

    /**
     * 条件のヒットするレコード取得
     *
     * @param array $condition 条件
     * @param string $order ソート順
     */
    public static function select($condition = [], $order = 'asc', $limit = null, $offset = null)
    {
        if (static::$table === null) {
            throw self::nothingTableException();
        }

        $q = DB::select()->from(static::$table)->where($condition);
        if ($order) {
            $q->order_by('id', $order);
        }
        if ($limit !== null) {
            $q->limit($limit);
        }
        if ($offset !== null) {
            $q->offset($offset);
        }
        // \Log::debug(print_r($q->compile(), true));
        $results = $q->execute()->as_array();

        return $results;
    }

    /**
     * 条件のヒットするレコード全件取得
     *
     * @param mix array $s / 条件 int $s ID
     */
    public static function current($s = [])
    {
        if (static::$table === null) {
            throw self::nothingTableException();
        }

        if (is_numeric($s)) {
            $param['id'] = $s;
        } else {
            $param = $s;
        }

        $results = DB::select()->from(static::$table)->where($param)->execute()->current();

        return $results;
    }

    /**
     *  レコードの件数を取得
     *
     *  @param array $conditions 条件
     *  @return int 件数
     */
    public static function count($conditions = [])
    {
        if (static::$table === null) {
            throw self::nothingTableException();
        }

        $result = DB::select(DB::expr('COUNT(' . self::$primary_key . ') as count'))
            ->from(static::$table)
            ->where($conditions)
            ->execute()
            ->current();

        return intval($result['count']);
    }

    /**
     * insert
     *
     * @param   string  $table  テーブル名
     * @param   array   $val    データ
     * @param boolean $last_insert_flag  last_insert_idがいるか
     */
    public static function insert($val, $last_insert_flag = false)
    {
        if (DBUtil::field_exists(static::$table, ['created_at'])) {
            $val['created_at'] = date('Y-m-d H:i:s');
        }
        if (DBUtil::field_exists(static::$table, ['updated_at'])) {
            $val['updated_at'] = '0000-00-00 00:00:00';
        }
        $query = DB::insert(static::$table);
        $query->set($val);
        $query->execute();

        if (Fuel::$env == 'development') {
           Log::debug(DB::last_query());
        }

        if ($last_insert_flag == true) {
            $query = DB::query('SELECT last_insert_id() AS id');
            $res = $query->execute();

            return $res[0]['id'];
        } else {
            return true;
        }
    }

    /**
     *  バルクインサート
     *
     *  @param array $columns カラム名の配列
     *  @param array $values 値の配列
     *  @return boolean
     */
    public static function insertAny($columns, $values)
    {
        if (static::$table === null) {
            throw self::nothingTableException();
        }
        $existsCTIME = DBUtil::field_exists(static::$table, ['created_at']);

        if ($existsCTIME) {
            $columns[] = 'created_at';
        }

        $query = DB::insert(static::$table)
            ->columns($columns);

        foreach ($values as $v) {
            if ($existsCTIME) {
                $v['created_at'] = date('Y-m-d H:i:s');
            }
            $query->values($v);
        }

        $query->execute();

        return true;
    }

    /**
     * update
     *
     * @param   array   $val   データ
     * @param   array   $condition     条件
     */
    public static function update($val, $condition)
    {
        if (DBUtil::field_exists(static::$table, ['updated_at'])) {
            $val['updated_at'] = date('Y-m-d H:i:s');
        }
        $query = DB::update(static::$table);
        $query->set($val);
        $query->where($condition);
        $query->execute();

        return true;
    }

    /**
     * delete
     * @param mix array $s / 条件 int $s ID
     * @param bool $physical 物理削除
     */
    public static function delete($s = [], $physical = false)
    {
        if (static::$table === null) {
            throw self::nothingTableException();
        }

        if (is_numeric($s)) {
            $param['id'] = $s;
        } else {
            $param = $s;
        }

        // 物理削除
        if ($physical) {
            DB::delete(static::$table)->where($param)->execute();
        // 論理削除
        } else {
            $val['deleted_at'] = date('Y-m-d H:i:s');
            DB::update(static::$table)->set($val)->where($param)->execute();
        }


        return true;
    }

    /**
     *  テーブル名が設定されていない場合の LogicException オブジェクトを返す
     */
    private static function nothingTableException()
    {
        return new LogicException(get_called_class() . ' の table が設定されていません');
    }
}
