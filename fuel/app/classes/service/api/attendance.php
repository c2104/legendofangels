<?php

class Service_Api_Attendance extends Service_La
{
    /**
     * リハーサルの出欠を取得
     */
    public static function getList($when, $requestor)
    {
        // レスポンスパラメータを設定
        $parameter = self::getResponseParameter($requestor);

        // スケジュールを取得
        $schedules = self::getSchedule($when);
        if (!$schedules) {
            return [$parameter => 'と思ったけど、スケジュールがないれじぇ。'];
        }

        $list[$parameter] = '';
        foreach ($schedules as $key => $schedule) {
            // 該当日程の出欠をメンバー情報とともに取得
            $attendances = Model_Attendance::getListIncludeMemberInfo($schedule['id']);

            // 通知用にフォーマット
            $list[$parameter] .= self::formatBandList($schedule['date'], $schedule['event'], $attendances, $requestor);
        }

        return $list;
    }

    /**
     * 個人の出欠を取得
     */
    public static function getPersonalList($member_id, $period, $requestor)
    {
        // レスポンスパラメータを設定
        $parameter = self::getResponseParameter($requestor);

        // 出欠を取得
        $attendances = Model_Attendance::getPersonalList($member_id, $period);
        if (!$attendances) {
            return [$parameter => 'と思ったけど、出欠が登録されてないれじぇ。'];
        }

        // 通知用にフォーマット
        $list[$parameter] = self::formatPersonalList($member_id, $attendances, $requestor);

        return $list;
    }

    /**
     * 指定したリハーサルのスケジュールを取得
     */
    private static function getSchedule($when)
    {
        $today = date('Y-m-d');

        // 今日より後の一番違いリハーサルの日程を取得
        if ($when == 'next') {
            $where = ['date' => ['date', '>', $today]];
            $order = 'asc';
            $limit = 1;
            return Model_Schedule::select($where, $order, $limit);
        } elseif ($when == 'tw') {
            $where = ['date' => ['date', 'between', [$today, date('Y-m-d', strtotime($today.'+ 6days'))]]];
            return Model_Schedule::select($where);
        }

        return [];
    }

    /**
     * 出欠一覧をテキスト形式に変換
     * @param int $member_id メンバーID
     * @param array  $attendances 出欠一覧
     * @param string $requestor リクエスト元サービス名
     * @return string
     */
    private static function formatPersonalList($member_id, $attendances, $requestor)
    {
        // 名前を取得
        $member = Model_Member::current($member_id);
        $name = $member['last_name'].$member['first_name'];

        $list = $name."\n\n";
        foreach ($attendances as $key => $value) {
            // イベントを設定
            $event = ($value['event'] != '') ? $value['event'] : '通常練習';

            // 出欠状況を設定
            $attendance = (!$value['attendance_id']) ? '(未入力)' : Config::get('membercfg.attendance.'.$value['attendance_id']);

            // リストに追加
            $list .= $value['date'].' '.$event.' '.$attendance."\n";
        }

        $list = self::formatTag($list, $requestor);
        return $list;
    }

    /**
     * 出欠一覧をテキスト形式に変換
     * @param array  $attendances 出欠一覧
     * @param string $requestor リクエスト元サービス名
     * @return string
     */
    private static function formatBandList($date, $event, $attendances, $requestor)
    {
        // 日付とイベント名を追加
        $event = ($event != '') ? $event : '通常練習';

        // タイトル
        $list = '【'.$date.' '.$event."】\n";

        $tmp_attendance_id = '';
        foreach ($attendances as $key => $member) {
            // 出欠ステータスの変化を確認
            if ($member['attendance_id'] != $tmp_attendance_id) {
                $tmp_attendance_id = $member['attendance_id'];
                $list .= "\n";
            }

            // セクション名を取得
            foreach (Config::get('membercfg.section') as $key => $section) {
                if ($member['section_id'] == 0) {
                    $section_name = 'other';
                } elseif ($member['section_id'] == $section['id']) {
                    $section_name = $section['short'];
                }
            }

            // パート名を取得
            foreach (Config::get('membercfg.part') as $key => $part) {
                if ($member['part_id'] == 0) {
                    $part_name = 'other';
                } elseif ($member['part_id'] == $part['id']) {
                    $part_name = $part['short'];
                }
            }

            // 出欠状況を取得
            $attendance = (!$member['attendance_id']) ? '(未入力)' : Config::get('membercfg.attendance.'.$member['attendance_id']);

            // リストに追加
            $list .= $section_name.' '.$part_name.' '
                          .$member['last_name'].$member['first_name'].' '
                          .$attendance."\n";
        }

        $list = self::formatTag($list, $requestor);
        return $list;
    }

    /**
     * リクエスト元ごとのフォーマットタグを設定
     */
    private static function formatTag($data, $requestor)
    {
        switch ($requestor) {
            case 'slack':
                $data = "```".$data."```\n";
                break;
            case 'line':
                $data = $data."\n";
                break;
        }

        return $data;
    }

    /**
     * リクエスト元ごとのリクエストパラメータを取得
     */
    public static function getRequestParameter($requestor)
    {
        $parameter = '';
        switch ($requestor) {
            case 'slack':
                $parameter = 'text';
                break;
            case 'line':
                $parameter = 'when';
                break;
        }

        return $parameter;
    }

    /**
     * リクエスト元ごとのレスポンスパラメータを取得
     */
    public static function getResponseParameter($requestor)
    {
        $parameter = '';
        switch ($requestor) {
            case 'slack':
                $parameter = 'text';
                break;
            case 'line':
                $parameter = 'message';
                break;
        }

        return $parameter;
    }
}

