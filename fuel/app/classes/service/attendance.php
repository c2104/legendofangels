<?php

class Service_Attendance extends Service_La
{
    /**
     * スケジュールの出欠件数を取得
     * @param  int   $schedule_id スケジュールID
     * @param  array $attendee_count 集計データ
     * @return array
     */
    public static function getAttendeeCount($schedule_id = null, $attendee_count = [])
    {
        $count = DB::select('attendance_id', DB::expr('count(`attendance_id`) as cnt'))->from('attendance')
                    ->where('schedule_id', '=', $schedule_id)
                    ->group_by('attendance_id')
                    ->execute()->as_array('attendance_id');

        foreach (Config::get('membercfg.attendance') as $attend_id => $attend) {
            // 未定は集計しない
            if ($attend_id < 9) {
                $attendee_count[$schedule_id][$attend_id] = isset($count[$attend_id]) ? $count[$attend_id]['cnt'] : 0;
            }
        }

        return $attendee_count;
    }

    public static function setInvalidAttendeeToPastSchedule($member_id, $year = null)
    {
        // 今期の過去のスケジュールを取得
        $past_schedule = Model_Schedule::getSchedules('past', $year);

        // 過去のスケジュールがある場合
        if ($past_schedule) {
            $columns = ['member_id', 'schedule_id', 'attendance_id'];
            $values = [];

            // 登録する出欠IDを無効に設定
            $attendance_id = 10;

            foreach ($past_schedule as $schedule_id => $schedule) {
                $values[] = [$member_id, $schedule_id, $attendance_id];
            }

            Model_Attendance::insertAny($columns, $values);
        }
    }

    /**
     * メンバーごとの出席率を皆勤を取得
     * @param $array $attendance_count_list メンバーごとの出欠数  [$member_id => ['attendance_count' => $attendance_count]]
     * @param $int $all_schedule_count 出欠数をカウントする全日程数
     * @param $bool $include_perfect_attendance 皆勤の有無を含めるか
     * @return array
     */
    public static function getAttendanceRate($attendance_count_list, $all_schedule_count)
    {
        $attendance_rate_list = [];
        foreach ($attendance_count_list as $member_id => $attendance) {
            $attend_count     = isset($attendance[Const_Attendance::ATTEND]) ? $attendance[Const_Attendance::ATTEND]['attendance_count'] : 0;
            $absence_count    = isset($attendance[Const_Attendance::ABSENCE]) ? $attendance[Const_Attendance::ABSENCE]['attendance_count'] : 0;
            $late_count       = isset($attendance[Const_Attendance::LATE]) ? $attendance[Const_Attendance::LATE]['attendance_count'] : 0;
            $leave_ealy_count = isset($attendance[Const_Attendance::LEAVE_EALY]) ? $attendance[Const_Attendance::LEAVE_EALY]['attendance_count'] : 0;
            $undecided_count  = isset($attendance[Const_Attendance::UNDECIDED]) ? $attendance[Const_Attendance::UNDECIDED]['attendance_count'] : 0;
            $none_count       = isset($attendance[Const_Attendance::NONE]) ? $attendance[Const_Attendance::NONE]['attendance_count'] : 0;
            $registration_schedule_count = $attend_count + $absence_count + $late_count + $leave_ealy_count + $undecided_count + $none_count;

            // 「出席」のみでの出席率
            // ただし、入団前のスケジュールは出席とみなす
            $only_attend_count = $attend_count + $none_count;
            $rate_only_attend  = ($only_attend_count / $registration_schedule_count) * 100;

            // 「出席」「遅刻」「早退」を含めた出席率
            // ただし、入団前のスケジュールは出席とみなす
            $full_attend_count = $attend_count + $late_count + $leave_ealy_count + $none_count;
            $rate_full_attend  = ($full_attend_count / $registration_schedule_count) * 100;

            $attendance_rate_list[$member_id] = [
                'only_attend' => round($rate_only_attend, 1),
                'full_attend' => round($rate_full_attend, 1),
            ];

            // 皆勤の有無
            $attendance_rate_list[$member_id]['perfect_attendance'] = ($rate_only_attend == 100);
        }

        // 「出席」のみの出席率の降順に並べ替え
        uasort($attendance_rate_list, function ($a, $b) {
            // 「出席」のみの出席率が同じ場合
            if ($a['only_attend'] == $b['only_attend']) {
                if ($a['full_attend'] == $b['full_attend']) {
                    return 0;
                } else {
                    return $a['full_attend'] < $b['full_attend'] ? 1 : -1;
                }
            // 「出席」のみの出席率が違う場合
            } else {
                return $a['only_attend'] < $b['only_attend'] ? 1 : -1;
            }
        });

        return $attendance_rate_list;
    }
}
