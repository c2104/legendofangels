<?php

Config::load('membercfg', true);

class Service_Member extends Service_La
{
    /**
     * 新規メンバーを追加
     * @param  int $position_id ポジションID
     * @param  int $developer   管理者権限
     * @return int
     */
    public static function addNewMember($profile)
    {
        $password       = 'la'.$profile['join_year'];
        $profile_fields = [
            'last_name'       => $profile['last_name'],
            'first_name'      => $profile['first_name'],
            'last_name_kana'  => $profile['last_name_kana'],
            'first_name_kana' => $profile['first_name_kana'],
            'developer'       => 0,
        ];
        $member_id = Auth::create_user($profile['login_id'], $password, $profile_fields);

        // 参加年度を登録
        $profile['member_id'] = $member_id;
        Model_JoinYear::setJoinYear($profile);

        // 該当年度のポジションを登録
        Model_Position::setPosition($profile);

        $msg =   '【メンバー】'.$profile['last_name'].$profile['first_name']."\n"
                .'【ログインID】'.$profile['login_id']."\n"
                .'【初期パスワード】'.$password;

        Session::set('add_msg', $msg);

        return $member_id;
    }

    /**
     * メンバー名をもとにメンバーの情報を取得
     * @param string $name メンバー名
     *  -  「姓 名」の形で指定する
     */
    public static function getInformationByName($name)
    {
        // 全角スペースを半角スペースに変換
        $name = str_replace('　', ' ', $name);

        // 前後のスペース削除（trimの対象半角スペースのみなので半角スペースに変換後行う）
        $name = trim($name);

        // 連続する半角スペースを半角スペースひとつに変換
        $name = preg_replace('/\s+/', ' ', $name);

        // 姓と名で分割
        $name = explode(' ', $name);

        // 正常に分割できていない場合はエラー
        if (empty($name[0]) || empty($name[1])) {
            return ['error' => "教えてくれた名前がわからなかったれじぇ。\n名前は「姓 名」の形で教えてほしいれじぇ。"];
        }

        // 姓名をもとに情報を取得
        $last_name  = $name[0];
        $first_name = $name[1];
        $member = Model_member::current([
            'last_name'  => $last_name,
            'first_name' => $first_name
        ]);

        // 正常に取得できなかった場合はエラー
        if (!$member) {
            return ['error' => "教えてくれた名前の登録がなかったれじぇ。\n漢字などが間違っていないか確認してほしいれじぇ。"];
        }

        return $member;
    }
}

