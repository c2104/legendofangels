<?php

Config::load('membercfg', true);

class Service_Year extends Service_La
{
    /**
     * 現年度を取得
     * @return int
     */
    public static function getCurrentYear()
    {
        $next_schedule = Model_Schedule::getNextSchedule();
        if (!$next_schedule) {
            return date('Y');
        }

        return (int)$next_schedule['year'];
    }

    /**
     * 画面表示する年度を取得
     * @param int $year
     * @return int
     */
    public static function getYearForScreen($year)
    {
        return $year ?? Service_Year::getCurrentYear();
    }

    /**
     * 発足年度から現在までのすべての年度一覧を取得
     * @return array
     */
    public static function getYearListFromStartYear()
    {
        $current_year = (int)self::getCurrentYear();

        // 現在の年度から発足年度までを降順で取得
        for ($i = $current_year; $i >= Config::get('start_year'); $i--) {
            $year_list[$i] = $i;
        }

        return $year_list;
    }

    /**
     * セレクトボックスに使用するための年度配列を取得
     * @return array
     */
    public static function getYearListForSelectBox($member_id, $is_developer)
    {
        $year_list = [];

        // 開発者の場合、発足年度から現在までのすべての年度を取得する
        if ($is_developer) {
            $year_list = self::getYearListFromStartYear();
            if (date('n') == Config::get('start_month')-1) {
                $this_year = date('Y');
                $year_list = [$this_year => $this_year] + $year_list;
            }
        // 開発者でない場合、参加していた年度のみ取得する
        } else {
            $join_year_list = Model_JoinYear::getJoinYear($member_id);
            foreach ($join_year_list as $key => $join_year) {
                $join_year = (int)$join_year['join_year'];
                $year_list[$join_year] = $join_year;
            }
        }

        return $year_list;
    }
}
