<?php

class Service_Login extends Service_La
{
    /**
     * はじめてのログインか
     * @param  strinng $last_login
     * @return bool
     */
    public static function isFirstLogin($last_login)
    {
        return $last_login === '0000-00-00 00:00:00';
    }
}
