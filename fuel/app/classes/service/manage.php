<?php

Config::load('membercfg', true);

class Service_Manage extends Service_La
{
    /**
     * マネージメントメニューの表示確認
     * @param  int $member_id メンバーID
     * @param  int $position_id ポジションID
     * @param  int $developer   管理者権限
     * @return bool
     */
    public static function isDisplayManageMenu($member_id = null, $position_id = null, $developer = null)
    {
        $member_ids = [
        ];

        // 表示するポジションであるか
        $position_ids = [
            Config::get('membercfg.position.create.id'),
            Config::get('membercfg.position.manage.id'),
            Config::get('membercfg.position.director.id'),
        ];
        $has_position_id = false;
        foreach ($position_ids as $key => $value) {
            if (in_array($value, $position_id)) {
                $has_position_id = true;
                break;
            }
        }

        if (in_array($member_id, $member_ids) || $has_position_id || $developer) {
            return true;
        }

        return false;
    }

    /**
     * メンバーリストページの各メンバープロフページへのリンクの表示確認
     * @param  int $position_id ポジションID
     * @param  int $developer   管理者権限
     * @return bool
     */
    public static function isDisplayMemberProfLink($position_id = null, $developer = null)
    {
        $prof_link = false;

        $position_ids = [
            Config::get('membercfg.position.create.id'),
            Config::get('membercfg.position.manage.id')
        ];

        if (in_array($position_id, $position_ids) || $developer) {
            $prof_link = true;
        }

        return $prof_link;
    }
}

