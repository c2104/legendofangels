<?php
/**
 * Part of the Fuel framework.
 *
 * @package    Fuel
 * @version    1.8
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2016 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The core of the framework.
 *
 * @package     Fuel
 * @subpackage  Core
 */
class Fuel extends Fuel\Core\Fuel
{
    /**
     * @var  string  constant used for when in local
     */
    const LOCAL = 'local';
}
