<?php

class Controller_Index extends Controller_Auth
{
    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        // メンバー情報
        $member = Model_Member::current($this->member_id);
        $this->data['member'] = $member;

        $condition   = ['year' => $this->current_year];
        $infomations = Model_Infomation::select($condition, 'desc');
        $this->data['infomations'] = $infomations;
        $this->template->title = 'メンバーページ';
    }

    public function after($response)
    {
        $this->template->css     = basename(__FILE__, '.php');
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
