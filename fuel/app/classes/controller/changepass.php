<?php

class Controller_Changepass extends Controller_Auth
{
    private $fields = ['old_pass', 'new_pass', 'new_pass_confirm'];

    public function action_index()
    {
        if (Input::post('confirm')) {
            foreach ($this->fields as $field) {
                Session::set_flash($field, Input::post($field));
            }

            $val = Validation::forge();
            $val->add_field('old_pass', '現在のパスワード', 'trim|required');
            $val->add_field('new_pass', '新しいパスワード', 'trim|required|min_length[6]|max_length[100]|match_pattern[/^[0-9a-zA-Z_%#-]+$/]');
            $val->add_field('new_pass_confirm', '新しいパスワード(確認)', 'trim|required|match_field[new_pass]');

            if ($val->run() && Security::check_token()) {
                $vars = $val->validated();
                if ($this->checkPasswordError($vars['old_pass'], $vars['new_pass'])) {
                    Session::set_flash('info', 'パスワードの変更が完了しました。');
                    if (Auth::get('last_login') === '0000-00-00 00:00:00') {
                        $val       = ['last_login' => Date::forge()->format('mysql')];
                        $condition = ['id' => $this->member_id];
                        Model_Member::update($val, $condition);
                        Response::redirect('/');
                    }
                }
            } else {
                Session::set_flash('error', $val->show_errors());
            }
        }

        $this->template->title = 'パスワード変更';
    }

    /**
     * パスワードチェック
     * @param string $old_password
     * @param string $new_password
     * @return bool
     */
    private function checkPasswordError(string $old_password, string $new_password): bool
    {
        if ($old_password == $new_password) {
            Session::set_flash('error', '現在と同じパスワードは設定できません。');
            return false;
        }
        if (!Auth::change_password($old_password, $new_password, Auth::get('login_id'))) {
            Session::set_flash('error', '現在のパスワードに誤りがあります。');
            return false;
        }

        return true;
    }

    public function after($response)
    {
        $this->template->css     = basename(__FILE__, '.php');
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
