<?php

class Controller_Attendlist extends Controller_Auth
{
    public function action_index()
    {
        // セレクトボックに使用する年度一覧を取得
        $year_list = Service_Year::getYearListForSelectBox($this->member_id, $this->developer);
        $this->data['year_list'] = $year_list;

        // 画面表示する年度を取得
        $select_year = Service_Year::getYearForScreen(Input::get('year'));

        // メンバーリストを取得
        $member_list = Model_JoinYear::getMemberListBySelectyear($select_year);
        foreach ($member_list as $key => $member) {
            $member_list[$key]['attendance'] = Model_Attendance::select(['member_id' => $member['id']]);
        }

        // 出欠カウント変数
        $attendee_count = [];

        // period=allの場合は全件、それ以外は本日以降のスケジュールのみ取得
        $period = Input::get('period') ?? 'later';
        $schedule_list = Model_Schedule::getSchedules($period, $select_year);

        foreach ($schedule_list as $key => $schedule) {
            // 日時を整形
            $date = explode('-', $schedule['date']);
            $schedule_list[$key]['year']  = $date[0];
            $schedule_list[$key]['month'] = $date[1];
            $schedule_list[$key]['day']   = $date[2];

            // 該当日の出欠件数を取得
            $attendee_count = Service_Attendance::getAttendeeCount($schedule['id'], $attendee_count);
        }
        $this->data['attendee_count']   = $attendee_count;

        // メンバープロフページへのリンクの表示確認
        $this->data['prof_link'] = Service_Manage::isDisplayMemberProfLink($this->profile['position_id'], $this->developer);

        $this->data['member_list']      = $member_list;
        $this->data['year_list']        = $year_list;
        $this->data['schedule_list']    = $schedule_list;
        $this->data['attendance_list']  = Config::get('membercfg.attendance');

        $this->template->title   = '出欠一覧';
    }

    public function after($response)
    {
        $this->template->css     = basename(__FILE__, '.php');
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
