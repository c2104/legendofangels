<?php

class Controller_Profile extends Controller_Auth
{
    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        // メンバーIDを取得
        $member_id = Input::get('id');
        if (!$member_id) {
            $member_id = Auth::get('id');
        }

        // メンバープロフを見れないポジション、あるいは開発者でない場合はメンバーリストページへリダイレクト
        $prof_link = Service_Manage::isDisplayMemberProfLink($this->profile['position_id'], $this->developer);
        if (!$prof_link && $member_id != Auth::get('id')) {
            Session::set_flash('error', '他のメンバーのプロフィールを見ることができません');
            Response::redirect('/memberlist');
        }

        // 該当のメンバープロフィールを取得
        $this->data['profile'] = Model_Member::current($member_id);

        // 参加年度
        $join_year_list = Model_JoinYear::getJoinYear($member_id);
        $this->data['join_year_list'] = $join_year_list;

        // 所属年数を取得
        $join_count = 0;
        foreach ($join_year_list as $join_year) {
            if ($join_year['position_id'] != Config::get('membercfg.position.support.id')) {
                $join_count++;
            }
        }
        $support_count = count($join_year_list) - $join_count;
        $this->data['join_count']    = $join_count;
        $this->data['support_count'] = $support_count;

        // ポジション、セクション、ポジションの一覧を取得
        $positions = Config::get('membercfg.position');
        $sections  = Config::get('membercfg.section');
        $parts     = Config::get('membercfg.part');
        $this->data['positions'] = $positions;
        $this->data['sections']  = $sections;
        $this->data['parts']     = $parts;

        $this->template->title   = 'プロフィール';
    }

    public function after($response)
    {
        $this->template->css     = basename(__FILE__, '.php');
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
