<?php

class Controller_api_Attendance extends Controller_Rest
{
    protected $format = 'json';

    public function post_change()
    {
        $hash           = Input::post('hash');
        $schedule_id    = Input::post('schedule_id');
        $attend_id      = Input::post('attend_id');
        $new_attend_id  = Input::post('new_attend_id');

        // メンバーIDを取得
        $condition = ['hash' => $hash];
        $member    = Model_Member::current($condition);

        // ステータスを変更
        $update_val = ['attendance_id' => $new_attend_id];
        $condition  = ['member_id'     => $member['id'],
                       'schedule_id'   => $schedule_id];

        Model_Attendance::update($update_val, $condition);

        // 更新情報を設定
        $current_year = Service_Year::getCurrentYear();
        $schedule     = Model_Schedule::current(['id' => $schedule_id]);

        $day_of_week  = Const_Date::DOW[date('w', strtotime($schedule['date']))];
        $name         = $member['last_name'].$member['first_name'];
        $attendance   = Config::get('membercfg.attendance');
        $infomation   = $name."が出欠を変更しました。\n"
                       .$schedule['date']."(".$day_of_week.")\n"
                       .$attendance[$attend_id]
                       .' → '
                       .$attendance[$new_attend_id];

        // 更新情報を保存
        $values = [
            'member_id'  => $member['id'],
            'year'       => $current_year,
            'infomation' => $infomation,
        ];
        Model_Infomation::insert($values);

        $res = '出欠を変更しました。';
        return $this->response($res);
    }

    public function get_list()
    {
        $res = [];
        $requestor = Input::get('requestor');
        if (!$requestor) {
            return $this->response(['message' => 'Error - 不正なアクセスです']);
        }

        // リクエスト元のリクエストパラメータを取得
        $parameter = Service_Api_Attendance::getRequestParameter($requestor);
        $when = Input::get($parameter);

        // 指定がない場合は今週の予定の出欠を取得
        if (!$when) {
            $when = 'tw';
        }

        $res = Service_Api_Attendance::getList($when, $requestor);
        return $this->response($res);
    }
}
