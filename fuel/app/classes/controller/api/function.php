<?php

class Controller_Api_Function extends Controller_Rest
{
    protected $format = 'json';

    public function post_change_developer()
    {
        $member_id  = Input::post('member_id');
        $dev_status = Input::post('dev_status');

        if ($dev_status == 1) {
            $dev_status = false;
        } else {
            $dev_status = true;
        }

        $set   = ['developer' => $dev_status];
        $where = ['id' => $member_id];
        Model_Member::update($set, $where);

        return $this->response();
    }

}