<?php

class Controller_Api_Line extends Controller_Rest
{
    protected $format = 'json';

    public function get_routing()
    {
        $user_id = Input::get('user_id');
        $message = urldecode(Input::get('message'));

        // 全体の出欠一覧を取得
        if ($message == 'バンド出欠確認') {
            $list = self::getBandAttendanceList($user_id);
            if (!empty($list['error'])) {
                $res = LineMessage::setResponse($list['error']);
                return $this->response($res);
            }
            $res  = LineMessage::setResponse($list);

        // 個人の今日以降の出欠登録状況を確認
        } elseif ($message == '出欠確認（今日以降）') {
            $list = self::getPersonalAttendanceList($user_id, 'later');
            if (!empty($list['error'])) {
                $res = LineMessage::setResponse($list['error']);
                return $this->response($res);
            }
            $res  = LineMessage::setResponse($list);

        // 個人の全ての出欠登録状況を確認
        } elseif ($message == '出欠確認（全て）') {
            $list = self::getPersonalAttendanceList($user_id, 'all');
            if (!empty($list['error'])) {
                $res = LineMessage::setResponse($list['error']);
                return $this->response($res);
            }
            $res  = LineMessage::setResponse($list);

        // 個人の出欠を登録
        } elseif (preg_match('/出欠登録(.*)/us', $message, $m)) {
            $result = self::setAttendance($user_id, $m);
            if (!empty($result['error'])) {
                $res = LineMessage::setResponse($result['error']);
                return $this->response($res);
            }
            $res = LineMessage::setResponse($result);

        // 個人の出欠を変更
        } elseif (preg_match('/出欠変更(.*)/us', $message, $m)) {
            $result = self::changeAttendance($user_id, $m);
            if (!empty($result['error'])) {
                $res = LineMessage::setResponse($result['error']);
                return $this->response($res);
            }
            $res = LineMessage::setResponse($result);

        // 利用登録処理
        } elseif (preg_match('/利用登録(.*)/u', $message, $m)) {
            $registration = self::registration($user_id, $m);
            if (!empty($registration['error'])) {
                $res = LineMessage::setResponse($registration['error']);
                return $this->response($res);
            }
            $res = LineMessage::setResponse($registration);

        } else {
            $res = LineMessage::setResponse('わからないれじぇ');
        }

        return $this->response($res);
    }

    /**
     * 全体の出欠一覧を取得
     */
    private function getBandAttendanceList($user_id)
    {
        // LINEユーザーIDを確認
        $user = LineMessage::checkUserId($user_id);
        if (!Model_Member::isDeveloper($user['member_id'])) {
            $check = self::checkError($user);
            if (!empty($check['error'])) {
                return $check['error'];
            }
        }

        // 今週の予定を取得
        $when = 'tw';
        $requestor = 'line';
        $result = Service_Api_Attendance::getList($when, $requestor);

        // レスポンスを設定
        $messages = [
            '今週のスケジュールの出欠れじぇ！',
            $result['message']
        ];

        return $messages;
    }

    /**
     * 出欠登録状況を確認
     */
    private function getPersonalAttendanceList($user_id, $period)
    {
        // LINEユーザーIDを確認
        $user = LineMessage::checkUserId($user_id);
        $check = self::checkError($user);
        if (!empty($check['error'])) {
            return $check['error'];
        }

        $requestor = 'line';
        $result = Service_Api_Attendance::getPersonalList($user['member_id'], $period, $requestor);

        // レスポンスを設定
        $first_message = ($period == 'later') ? 'あなたのこれからの登録状況れじぇ！' : 'あなたの全ての登録状況れじぇ！';
        $messages = [
            $first_message,
            $result['message']
        ];

        return $messages;
    }

    /**
     * 個人の出欠を登録
     */
    private function setAttendance($user_id, $message)
    {
        // LINEユーザーIDを確認
        $user = LineMessage::checkUserId($user_id);
        $check = self::checkError($user);
        if (!empty($check['error'])) {
            return $check['error'];
        }

        $member_id = $user['member_id'];

        // 日付の指定がない場合は手順を返信
        $date_list_str = $message[1];
        if (!$date_list_str || $date_list_str == '\n') {
            return ["登録したい日付と出欠を例のような形で教えてれじぇ。\n\n【例】\n出欠登録\n7/15 出席\n7/22 欠席\n7/28 未定\n8/5 遅刻\n8/12 早退"];
        }

        // 今期の出欠を取得
        $attendances = Model_Attendance::getPersonalList($member_id, 'all');

        // 今期の初回練習データを取得
        $first_schedule = Model_Schedule::getFirstSchedule();

        // 行を分割。先頭に空要素ができるので、array_filterで空要素を削除する。
        $date_list = explode('\n', $date_list_str);
        $date_list = array_filter($date_list, 'strlen');

        $result = '';
        foreach ($date_list as $key => $value) {
            // 日付と出欠を分割
            $value_array = explode(' ', $value);
            $date = $value_array[0];
            $attendance_name = $value_array[1];

            // 日付をフォーマット
            $date = date('Y-m-d', strtotime($date));
            if ($date < $first_schedule['date']) {
                $date = date('Y-m-d', strtotime($date.' + 1 year'));
            }

            // 日付のスケジュールがあるか確認
            $schedule = Model_Schedule::current(['date' => $date]);

            // スケジュールがない場合はその旨を通知
            if (!$schedule) {
                $result .= $date." × スケジュールがないれじぇ\n";
                continue;
            }

            // 出欠が登録済みの場合はその旨を通知
            $index = array_search($date, array_column($attendances, 'date'));
            if ($index && $attendances[$index]['attendance_id']) {
                $attendance_id = $attendances[$index]['attendance_id'];
                $attendance = Config::get('membercfg.attendance.'.$attendance_id);
                $result .= $date." × 既に ".$attendance." で登録済みれじぇ\n";
                continue;
            }

            // 出欠IDを取得
            $attendance_id = array_search($attendance_name, Config::get('membercfg.attendance'));

            // 出欠を登録
            $data = [
                'member_id' => $member_id,
                'schedule_id' => $schedule['id'],
                'attendance_id' => $attendance_id
            ];
            Model_Attendance::insert($data);

            $result .= $date." ○ ".$attendance_name." で登録したれじぇ\n";
        }

        $messages = [
            "○になっているスケジュールの出欠登録が完了したれじぇ！",
            $result
        ];
        return $messages;
    }

    /**
     * 個人の出欠を変更
     */
    private function changeAttendance($user_id, $message)
    {
        // LINEユーザーIDを確認
        $user = LineMessage::checkUserId($user_id);
        $check = self::checkError($user);
        if (!empty($check['error'])) {
            return $check['error'];
        }

        $member_id = $user['member_id'];

        // 日付の指定がない場合は手順を返信
        $date_list_str = $message[1];
        if (!$date_list_str || $date_list_str == '\n') {
            return ["変更したい日付と出欠を例のような形で教えてれじぇ。\n\n【例】\n出欠変更\n7/15 出席\n7/22 欠席\n7/28 未定\n8/5 遅刻\n8/12 早退"];
        }

        // 今期の出欠を取得
        $attendances = Model_Attendance::getPersonalList($member_id, 'all');

        // 今期の初回練習データを取得
        $first_schedule = Model_Schedule::getFirstSchedule();

        // 行を分割。先頭に空要素ができるので、array_filterで空要素を削除する。
        $date_list = explode('\n', $date_list_str);
        $date_list = array_filter($date_list, 'strlen');

        $result = '';
        foreach ($date_list as $key => $value) {
            // 日付と出欠を分割
            $value_array = explode(' ', $value);
            $date = $value_array[0];
            $attendance_name = $value_array[1];

            // 日付をフォーマット
            $date = date('Y-m-d', strtotime($date));
            if ($date < $first_schedule['date']) {
                $date = date('Y-m-d', strtotime($date.' + 1 year'));
            }

            // 日付のスケジュールがあるか確認
            $schedule = Model_Schedule::current(['date' => $date]);

            // スケジュールがない場合はその旨を通知
            if (!$schedule) {
                $result .= $date." × スケジュールがないれじぇ\n";
                continue;
            }

            // 出欠が登録済みの場合はその旨を通知
            $index = array_search($date, array_column($attendances, 'date'));
            if ($index && $attendances[$index]['attendance_id']) {
                $attendance_id = $attendances[$index]['attendance_id'];
                $attendance = Config::get('membercfg.attendance.'.$attendance_id);
                if ($attendance_name == $attendance) {
                    $result .= $date." × 既に ".$attendance." で登録済みれじぇ\n";
                    continue;
                }
            }

            // 出欠IDを取得
            $attendance_id = array_search($attendance_name, Config::get('membercfg.attendance'));

            // 出欠を変更
            $data = [
                'attendance_id' => $attendance_id
            ];
            $where = [
                'member_id' => $member_id,
                'schedule_id' => $schedule['id'],
            ];
            Model_Attendance::update($data, $where);
            $result .= $date." ○ ".$attendance." から ".$attendance_name." に変更したれじぇ\n";

            $values = [
                'member_id'  => $member_id,
                'year'       => $current_year,
                'infomation' => $infomation,
            ];
            Model_Infomation::insert($values);

        }

        $messages = [
            "○になっているスケジュールの出欠変更が完了したれじぇ！",
            $result
        ];
        return $messages;
    }

    /**
     * 利用登録案内
     */
    private function registration($user_id, $message)
    {
        // LINEユーザーIDを確認
        $user = LineMessage::checkUserId($user_id);
        if ($user) {
            return ['error' => "もう利用登録済みれじぇ。\n出欠の登録や変更、確認ができるれじぇ！"];
        }

        // 名前がない場合は手順を返信
        $name = $message[1];
        if (!$name) {
            return ["あなたの名前をもとに登録をするから、メンバーページに登録されているあなたの名前を「利用登録 {姓} {名}」の形で教えてれじぇ。\n\n【例】\n利用登録 山田 太郎"];
        }

        // 名前がある場合は、名前をもとにメンバー情報を取得
        // 取得できない場合はその旨のエラーを返信
        $member = Service_Member::getInformationByName($name);
        if (!empty($member['error'])) {
            return $member;
        }

        // メンバーIDの登録が既にある場合はupdate、ない場合はinsert
        if (Model_LineApi::current(['member_id' => $member['id']])) {
            Model_LineApi::update(
                ['user_id'   => $user_id],
                ['member_id' => $member['id']]
            );
        } else {
            Model_LineApi::insert([
                'member_id' => $member['id'],
                'user_id'   => $user_id
            ]);
        }

        return ["登録が完了したれじぇ。\nこれで出欠の登録や変更、確認ができるれじぇ！"];
    }

    private function checkError($user)
    {
        if (!$user) {
            return ['error' => "まだ利用登録ができていないれじぇ。\nまずは「利用登録」と送信して、手順にしたがって登録してほしいれじぇ！"];
        }

        if (!JoinYear::isJoinedYear($user['member_id'])) {
            return ['error' => "あなたは今年度のLAに参加してないれじぇ！"];
        }

        return true;
    }
}
