<?php

class Controller_Addmember extends Controller_Auth
{

    private $fields = [
        'login_id', 'last_name', 'first_name',
        'last_name_kana', 'first_name_kana', 'join_year',
        'position_id', 'section_id', 'part_id'
    ];

    public function action_index()
    {
        // 管理メニューを表示しないポジションのメンバーには表示させない
        if (!$this->manage_menu) {
            Response::redirect(LaUri::baseUrl());
        }

        if (Input::post('confirm')) {
            foreach ($this->fields as $field) {
                Session::set_flash($field, Input::post($field));
            }

            $val = Validation::forge();
            $val->add_callable('Validate_myvalidation');

            $val->add_field('login_id', 'ログインID', 'trim|required|valid_email');
            $val->add_field('last_name', '姓', 'trim|required');
            $val->add_field('first_name', '名', 'trim|required');
            $val->add_field('last_name_kana', '姓（かな）', 'trim|required');
            $val->add_field('first_name_kana', '名（かな）', 'trim|required');
            $val->add_field('join_year', '年度', 'trim|required');
            $val->add_field('position_id', 'ポジション', 'trim')
                ->add_rule('valid_form_select');
            $val->add_field('section_id', 'セクション', 'trim');
            $val->add_field('part_id', 'パート', 'trim');

            if ($val->run() && Security::check_token()) {
                $vars = $val->validated();

                if (Model_Member::checkDuplicateLoginId($vars['login_id'])) {
                    Response::redirect('/'.$this->ctrl.'/confirm');
                } else {
                    Session::set_flash('error', 'そのログインID(メールアドレス)は既に使用されています');
                }
            } else {
                Session::set_flash('error', $val->show_errors());
            }
        }

        // 年度一覧を取得
        $year_list =Service_Year::getYearListFromStartYear();
        if (date('n') == Config::get('start_month') - 1) {
            $this_year = date('Y');
            $year_list = [$this_year => $this_year] + $year_list;
        }
        $this->data['year_list'] = $year_list;

        // セクションとポジションの一覧を取得
        $positions = [];
        $sections  = [];
        $parts     = [];
        foreach (Config::get('membercfg.position') as $value) {
            $positions[$value['id']] = $value['name'];
        };
        foreach (Config::get('membercfg.section') as $value) {
            $sections[$value['id']] = $value['name'];
        };
        foreach (Config::get('membercfg.part') as $value) {
            $parts[$value['id']] = $value['name'];
        };

        $this->data['positions'] = $positions;
        $this->data['sections']  = $sections;
        $this->data['parts']     = $parts;

        $this->template->title   = 'メンバー登録';
    }

    public function action_confirm()
    {
        foreach ($this->fields as $field) {
            $this->data[$field] = Session::get_flash($field);
            Session::keep_flash($field);
        }

        if (Input::post('registration')) {
            // メンバー情報を登録
            $member_id = Service_Member::addNewMember($this->data);

            // 今期の過去のスケジュールに無効を登録
            $year = $this->data['join_year'];
            Service_Attendance::setInvalidAttendeeToPastSchedule($member_id, $year);

            Response::redirect('/'.$this->ctrl.'/done');
        } elseif (Input::post('back')) {
            Response::redirect('/'.$this->ctrl);
        }

        $positions = Config::get('membercfg.position');
        foreach ($positions as $key => $position) {
            if ($position['id'] == $this->data['position_id']) {
                $this->data['position_name'] = $position['name'];
            }
        }

        $sections = Config::get('membercfg.section');
        foreach ($sections as $key => $section) {
            if ($section['id'] == $this->data['section_id']) {
                $this->data['section_name'] = $section['name'];
            }
        }

        $parts = Config::get('membercfg.part');
        foreach ($parts as $key => $part) {
            if ($part['id'] == $this->data['part_id']) {
                $this->data['part_name'] = $part['name'];
            }
        }

        $this->template->title = 'メンバー登録';
    }

    public function action_done()
    {
        $this->data['msg'] = Session::get('add_msg');
        $this->template->title = 'メンバー登録';
    }

    public function after($response)
    {
        $this->template->css     = basename(__FILE__, '.php');
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
