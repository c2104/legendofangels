<?php

class Controller_Schedule extends Controller_Auth
{
    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        // 管理メニューを表示しないポジションのメンバーには表示させない
        if (!$this->manage_menu) {
            Response::redirect('/');
        }

        $year = Input::get('year') ?? $this->current_year;
        $this->data['year'] = $year;

        // スケジュールを取得
        $schedules = Model_Schedule::getSchedules('all', $year);
        // 曜日を取得
        foreach ($schedules as $key => $schedule) {
            $schedules[$key]['dow'] = Commonfunc::getDayofweek($schedule['date']);
        }
        $this->data['schedules'] = $schedules;

        // 年度一覧を取得
        $year_list =Service_Year::getYearListFromStartYear();
        if (date('n') == Config::get('start_month') - 1) {
            $this_year = date('Y');
            $year_list = [$this_year => $this_year] + $year_list;
        }
        $this->data['year_list'] = $year_list;

        $this->template->title   = 'スケジュール一覧';
    }

    public function after($response)
    {
        $this->template->css     = $this->ctrl;
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
