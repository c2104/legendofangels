<?php

class Controller_Schedule_Add extends Controller_Auth
{
    private $fields = ['year', 'date', 'count_check', 'event'];

    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        // 管理メニューを表示しないポジションのメンバーには表示させない
        if (!$this->manage_menu) {
            Response::redirect('/');
        }

        if (Input::post('confirm')) {
            foreach ($this->fields as $field) {
                Session::set_flash($field, Input::post($field));
            }

            $val = Validation::forge();
            $val->add_callable('Validate_myvalidation');

            $val->add_field('year', '年度', 'trim|required');
            $val->add_field('date', '日程', 'trim|required');
            $val->add_field('count_check', '出欠カウント', 'trim');
            $val->add_field('event', '詳細', 'trim');

            if ($val->run() && Security::check_token()) {
                $vars = $val->validated();
                if (Model_Schedule::checkDuplicateSchedule($vars['date'])) {
                    Response::redirect('/'.$this->ctrl.'/confirm');
                } else {
                    Session::set_flash('error', 'その日程は既に登録されています');
                }
            } else {
                Session::set_flash('error', $val->show_errors());
            }
        } else {
            Session::set_flash('count_check', true);
        }

        // 年度一覧を取得
        $year_list =Service_Year::getYearListFromStartYear();
        if (date('n') == Config::get('start_month')-1) {
            $this_year = date('Y');
            $year_list = [$this_year => $this_year] + $year_list;
        }
        $this->data['year_list'] = $year_list;

        $this->template->title   = 'スケジュール登録';
    }

    public function action_confirm()
    {
        foreach ($this->fields as $field) {
            $this->data[$field] = Session::get_flash($field);
            Session::keep_flash($field);
        }

        if (Input::post('registration')) {
            // スケジュールを登録
            Model_Schedule::setSchedule($this->data);

            Response::redirect('/'.$this->ctrl.'/done');
        } elseif (Input::post('back')) {
            Response::redirect('/schedule/add');
        }

        $this->data['count_check_display'] = $this->data['count_check'] ? '有' : '無';

        $this->template->title = 'スケジュール登録';
    }

    public function action_done()
    {
        // $this->data['msg'] = '登録が完了しました';
        $this->data['msg'] = '';
        $this->template->title   = 'スケジュール登録';
    }

    public function after($response)
    {
        $this->template->css     = $this->ctrl;
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
