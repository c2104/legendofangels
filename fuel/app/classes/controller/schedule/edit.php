<?php

class Controller_Schedule_Edit extends Controller_Auth
{
    private $fields = ['year', 'date', 'count_check', 'event'];

    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        // 管理メニューを表示しないポジションのメンバーには表示させない
        if (!$this->manage_menu) {
            Response::redirect('/');
        }

        // スケジュールIDを取得
        $schedule_id = Input::get('id');
        $this->data['schedule_id'] = $schedule_id;

        if (Input::post('delete')) {
            Model_Schedule::delete(['id' => $schedule_id]);
            Session::set_flash('info', '削除しました');
            Response::redirect('/schedule');
        } elseif (Input::post('save')) {
            foreach ($this->fields as $field) {
                Session::set_flash($field, Input::post($field));
            }

            $val = Validation::forge();
            $val->add_callable('Validate_myvalidation');

            $val->add_field('count_check', '出欠カウント', 'trim');
            $val->add_field('event', '詳細', 'trim');

            if ($val->run() && Security::check_token()) {
                $vars = $val->validated();

                Model_Schedule::update($vars, ['id' => $schedule_id]);
                Session::set_flash('info', '保存が完了しました');
            } else {
                Session::set_flash('error', $val->show_errors());
            }
        } else {
            // スケジュールの指定がない場合は一覧へ
            if (!$schedule_id) {
                Response::redirect('/schedule');
            }
        }

        // スケジュールを取得、指定のスケジュールがない場合は一覧へ
        $schedule = Model_Schedule::current($schedule_id);
        if (!$schedule) {
            Response::redirect('/schedule');
        }

        // フォームに値をセット
        foreach ($this->fields as $key => $field) {
            Session::set_flash($field, $schedule[$field]);
        }

        // 年度一覧を取得
        $year_list =Service_Year::getYearListFromStartYear();
        if (date('n') == Config::get('start_month') - 1) {
            $this_year = date('Y');
            $year_list = [$this_year => $this_year] + $year_list;
        }
        $this->data['year_list'] = ['' => '▼ 選択してください'] + $year_list;

        $this->template->title   = 'スケジュール編集';
    }

    public function after($response)
    {
        $this->template->css     = $this->ctrl;
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
