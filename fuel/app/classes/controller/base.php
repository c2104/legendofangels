<?php

class Controller_Base extends Controller_Hybrid
{
    public $template = 'pc/template';
    public $device   = 'pc';
    public $sp       = '';
    public $data     = [];
    public $ctrl     = '';
    public $action   = '';
    public $current_year = '';
    public $auth     = null;

    public function before()
    {
        parent::before();

        // コントローラ名とアクション名を取得
        $controller   = strtolower(str_replace('Controller_', '', Request::main()->controller));
        $this->ctrl   = strtolower(str_replace('_', '/', $controller));
        $this->action = Request::active()->action;

        // デバイス判定
        $detect = new Mobile_Detect();
        if ($detect->isMobile() && !$detect->isTablet()) {
            $this->template = View::forge('sp/template', $this->data);
            $this->device = 'sp';
            $this->sp = 'sp';
        }
        $this->template->sp = $this->sp;
    }

    public function after($response)
    {
        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
