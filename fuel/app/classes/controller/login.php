<?php

class Controller_Login extends Controller_Base
{
    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        // ログイン済みの場合は、ログイン後トップへ
        Auth::check() and Response::redirect(Config::get('top_url'));

        if (Input::method() == 'POST') {
            $val = Validation::forge();
            $val->add_field('login_id', 'ログインID', 'trim|required|valid_email');
            $val->add_field('password', 'パスワード', 'trim|required');
            if ($val->run()) {
                $vars = $val->validated();

                // ログイン
                if (Auth::login($vars['login_id'], $vars['password'])) {
                    // 初ログインの場合は、パスワード設定画面へ
                    $last_login = Auth::get('last_login');
                    if (Service_Login::isFirstLogin($last_login)) {
                        Response::redirect('/changepass');
                    } else {
                        $val       = ['last_login' => Date::forge()->format('mysql')];
                        $condition = ['id' => Auth::get('id')];
                        Model_Member::update($val, $condition);

                        if (Session::get('latest_url')) {
                            Response::redirect(Session::get('latest_url'));
                        } else {
                            Response::redirect('/');
                        }
                    }
                } else {
                    Session::set_flash('error', 'ログインID、パスワードに誤りがあります');
                }
            } else {
                Session::set_flash('error', $val->show_errors());
            }
        }

        $this->template->title = 'ログイン';
    }

    public function after($response)
    {
        $this->template->css     = basename(__FILE__, '.php');
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
