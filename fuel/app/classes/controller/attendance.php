<?php

class Controller_Attendance extends Controller_Base
{

    private $fields = ['attendance'];
    private $hash   = '';

    public function before()
    {
        parent::before();

        $this->hash = Input::get('hash');
        if (!$this->hash) {
            Response::redirect(Config::get('top_url'));
        }
        $this->data['hash'] = $this->hash;
    }

    public function action_index()
    {
        // hashからメンバー情報を取得
        $member = Model_Member::current(['hash' => $this->hash]);
        $this->data['member_id'] = $member['id'];

        // 今年度のスケジュールを取得
        $schedules = Model_Schedule::getSchedules();
        $schedule_ids = array_column($schedules, 'id');

        // 今年度参加しているか確認
        $current_year = Service_Year::getCurrentYear();
        $is_joined = JoinYear::isJoinedYear($member['id'], $current_year);
        $this->data['is_joined'] = $is_joined;

        // 出欠を取得
        $member_attends = Model_Attendance::select([
            ['member_id', '=', $member['id']],
            ['schedule_id', 'in', $schedule_ids]
        ]);

        if (Input::post('submit')) {
            $val = Validation::forge();
            foreach ($schedules as $schedule) {
                Session::set_flash('attendance_'.$schedule['id'], Input::post('attendance_'.$schedule['id']));
                $val->add_field('attendance_'.$schedule['id'], '出欠', 'trim');
            }

            if ($val->run() && Security::check_token()) {
                $vars = $val->validated();

                $registered_schedule = Arr::pluck($member_attends, 'schedule_id');
                foreach ($schedules as $schedule) {
                    // 出欠がPOSTされて、かつ、まだ未登録のスケジュールの場合、新規登録
                    if ($vars['attendance_'.$schedule['id']]) {
                        if (!in_array($schedule['id'], $registered_schedule)) {
                            $insert_val = [
                                'member_id'     => $member['id'],
                                'schedule_id'   => $schedule['id'],
                                'attendance_id' => $vars['attendance_'.$schedule['id']],
                            ];
                            Model_Attendance::insert($insert_val);
                        }
                    }
                }
                Session::set_flash('info', '出欠登録が完了しました。');
            } else {
                Session::set_flash('error', $val->show_errors());
            }
        } else {
            if ($member_attends) {
                foreach ($member_attends as $value) {
                    Session::set_flash('attendance_'.$value['schedule_id'], $value['attendance_id']);
                }
            }
        }

        // 曜日を取得
        foreach ($schedules as $key => $schedule) {
            $schedules[$key]['dow'] = Commonfunc::getDayofweek($schedule['date']);
        }

        foreach ($member as $key => $value) {
            $this->data[$key] = $value;
        }
        $this->data['name'] = $member['last_name'].' '.$member['first_name'];

        // 出欠リストを取得
        $attendance = Config::get('membercfg.attendance');
        $attendance = ['' => '▼ 選択'] + $attendance;
        $this->data['attendance'] = $attendance;

        $this->data['current_year'] = $current_year;
        $this->data['schedules']    = $schedules;

        $this->template->title = '出欠申請';
    }

    public function after($response)
    {
        $this->template->css     = basename(__FILE__, '.php');
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
