<?php

class Controller_Addjoinyear extends Controller_Auth
{
    private $fields = ['member_id', 'join_year', 'position_id', 'section_id', 'part_id'];

    public function action_index()
    {
        // 管理メニューを表示しないポジションのメンバーには表示させない
        if (!$this->manage_menu) {
            Response::redirect(LaUri::baseUrl());
        }

        if (Input::post('confirm')) {
            foreach ($this->fields as $field) {
                Session::set_flash($field, Input::post($field));
            }
            foreach (Config::get('membercfg.position') as $key => $value) {
                $choice_position[$value['id']] = $value['name'];
            }

            $val = Validation::forge();
            $val->add_callable('Validate_myvalidation');

            $val->add_field('member_id', '登録メンバー', 'trim|required');
            $val->add_field('join_year', '年度', 'trim|required');
            $val->add('position_id', 'ポジション', ['type' => 'checkbox', 'options' => $choice_position])
                ->add_rule('required');
            $val->add_field('section_id', 'セクション', 'trim');
            $val->add_field('part_id', 'パート', 'trim');

            if ($val->run() && Security::check_token()) {
                $vars = $val->validated();

                if (Model_JoinYear::checkDuplicateJoinYear($vars['member_id'], $vars['join_year'])) {
                    Response::redirect('/'.$this->ctrl.'/confirm');
                } else {
                    Session::set_flash('error', 'その年度は既に登録されています');
                }
            } else {
                \Log::debug(print_r(Session::get_flash('position_id'), true));
                Session::set_flash('error', $val->show_errors());
            }
        }

        // メンバー情報
        $member_list = Model_Member::getNameList();
        $this->data['name_list'] = ['' => '▼ 選択してください'] + $member_list;

        // 年度一覧を取得
        $year_list =Service_Year::getYearListFromStartYear();
        if (date('n') == Config::get('start_month')-1) {
            $this_year = date('Y');
            $year_list = [$this_year => $this_year] + $year_list;
        }
        $this->data['year_list'] = $year_list;

        // セクションとポジションの一覧を取得
        $positions = Config::get('membercfg.position');
        // $positions = [];
        // foreach (Config::get('membercfg.position') as $key => $value) {
        //     $positions[$value['id']] = $value['name'];
        // }
        $sections  = [];
        foreach (Config::get('membercfg.section') as $key => $value) {
            $sections[$value['id']] = $value['name'];
        }
        $parts     = [];
        foreach (Config::get('membercfg.part') as $key => $value) {
            $parts[$value['id']] = $value['name'];
        }

        $this->data['positions'] = $positions;
        $this->data['sections']  = $sections;
        $this->data['parts']     = $parts;

        $this->template->title   = '参加年度登録';
    }

    public function action_confirm()
    {
        foreach ($this->fields as $field) {
            $this->data[$field] = Session::get_flash($field);
            Session::keep_flash($field);
        }

        if (Input::post('registration')) {
            // 参加年度を登録
            Model_JoinYear::setJoinYear($this->data);

            // 該当年度のポジションを登録
            Model_Position::setPosition($this->data);

            // 今期の過去のスケジュールに無効を登録
            $member_id = $this->data['member_id'];
            $year      = $this->data['join_year'];
            Service_Attendance::setInvalidAttendeeToPastSchedule($member_id, $year);

            Response::redirect('/'.$this->ctrl.'/done');
        } elseif (Input::post('back')) {
            Response::redirect('/addjoinyear');
        }

        $name_list = Model_Member::getNameList([$this->data['member_id']]);
        $this->data['name'] = $name_list[$this->data['member_id']];

        $positions = Config::get('membercfg.position');
        $this->data['position_name'] = '';
        echo print_r($this->data['position_id']).' ';
        foreach ($positions as $key => $position) {
            if (in_array($position['id'], $this->data['position_id'])) {
                $this->data['position_name'] .= ($this->data['position_name'] == '') ? $position['name'] : ', '.$position['name'];
            }
        }

        $sections = Config::get('membercfg.section');
        foreach ($sections as $key => $section) {
            var_dump($section['id'].' == '.print_r($this->data['section_id']));
            if ($section['id'] == $this->data['section_id']) {
                $this->data['section_name'] = $section['name'];
                break;
            }
        }

        $parts = Config::get('membercfg.part');
        foreach ($parts as $key => $part) {
            if ($part['id'] == $this->data['part_id']) {
                $this->data['part_name'] = $part['name'];
                break;
            }
        }

        $this->template->title = 'メンバー登録';
    }

    public function action_done()
    {
        $this->data['msg'] = '登録が完了しました';
        $this->template->title = 'メンバー登録';
    }

    public function after($response)
    {
        $this->template->css     = basename(__FILE__, '.php');
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
