<?php

class Controller_Auth extends Controller_Base
{
    public function before()
    {
        parent::before();

        // 最後に訪問したurlをセッションへ保存
        $latest_url = Uri::current();
        if (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING']) {
            $latest_url .= '?'.$_SERVER['QUERY_STRING'];
        }
        Session::set('latest_url', $latest_url);

        // ログインしていない場合は、ログイン画面へリダイレクト
        if (!Auth::check() && (Uri::string() != 'login')) {
            Response::redirect('/login');
        }

        $this->auth = Auth::instance('MemberAuth');
        $this->member_id = (int) Auth::get('id');
        $this->data['member_id'] = $this->member_id;

        // 管理者権限の有無を取得
        $this->developer = Auth::get('developer');
        $this->data['developer'] = $this->developer;

        if ($this->developer) {
            $this->data['positions'] = Config::get('membercfg.position');
        }

        // ログインしたメンバーの今年度の情報を取得
        $this->profile = Model_JoinYear::getCurrentYearPosition($this->member_id);
        $this->data['profile'] = $this->profile;

        // 今年度を取得
        $this->current_year =Service_Year::getCurrentYear();
        $this->data['current_year'] = $this->current_year;

        // 管理メニューの表示/非表示
        $this->manage_menu = false;
        if ($this->developer) {
            $this->manage_menu = true;
        } elseif ($this->profile) {
            $this->manage_menu = Service_Manage::isDisplayManageMenu($this->profile['member_id'], $this->profile['position_id'], $this->developer);
        }
        $this->data['manage_menu'] = $this->manage_menu;

        // 初ログインの場合は、パスワード設定画面へ
        if (Service_Login::isFirstLogin(Auth::get('last_login')) && Uri::string() != 'changepass') {
            Response::redirect('/changepass');
        }

        if ($this->device == 'pc') {
            $this->template->nav = View::forge($this->device.'/layout/nav.php', $this->data);
        }
    }

    public function after($response)
    {
        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
