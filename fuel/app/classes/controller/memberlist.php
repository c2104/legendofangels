<?php

class Controller_Memberlist extends Controller_Auth
{
    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        // セレクトボックに使用する年度一覧を取得
        $year_list = Service_Year::getYearListForSelectBox($this->member_id, $this->developer);
        $this->data['year_list'] = $year_list;

        // 画面表示する年度を取得
        $select_year = Service_Year::getYearForScreen(Input::get('year'));

        // メンバーリストを取得
        $member_list = Model_JoinYear::getMemberPositionList($select_year);
        $this->data['member_list'] = $member_list;

        // 各ポジションの人数を取得
        $position_count = Model_Position::getPositionCount($select_year);
        $this->data['position_count'] = $position_count;

        // メンバープロフページへのリンクの表示確認
        $this->data['prof_link'] = Service_Manage::isDisplayMemberProfLink($this->profile['position_id'], $this->developer);

        // セクションとポジションの一覧を取得
        $positions = Config::get('membercfg.position');
        if ($select_year < 2015) {
            unset($positions['section_leader']);
        }
        if ($select_year >= 2015) {
            unset($positions['instructor']);
            if ($select_year < 2019) {
                unset($positions['tech']);
            }
        }
        $sections = Config::get('membercfg.section');
        $parts = Config::get('membercfg.part');

        $this->data['positions'] = $positions;
        $this->data['sections'] = $sections;
        $this->data['parts'] = $parts;

        $this->template->title = 'メンバーリスト';
    }

    public function after($response)
    {
        $this->template->css     = basename(__FILE__, '.php');
        $this->template->js      = $this->ctrl.'/'.$this->action;
        $this->template->header  = View::forge($this->device.'/layout/header.php', $this->data);
        $this->template->footer  = View::forge($this->device.'/layout/footer.php', $this->data);
        $this->template->content = View::forge($this->device . '/'.$this->ctrl.'/'.$this->action.'.php', $this->data);

        $response = parent::after($response); // あなた自身のレスポンスオブジェクトを作成する場合は必要ありません。
        return $response; // after() は確実に Response オブジェクトを返すように
    }
}
