<?php

class Model_Attendance extends Model_La
{
    protected static $table = 'attendance';

    /**
     * 個人の出欠一覧を取得
     */
    public static function getPersonalList($member_id, $period = 'all', $year = null)
    {
        // 年度の指定がない場合は今期を設定
        if (!$year) {
            $year =Service_Year::getCurrentYear();
        }

        $query = DB::select('schedule.date', 'schedule.event', 'attendance_id')->from('schedule')
                    ->join(static::$table, 'left')
                        ->on('schedule.id', '=', static::$table.'.schedule_id')
                        ->on(static::$table.'.member_id', '=', DB::expr($member_id))
                    ->where('year', '=', $year)
                    ->where('deleted_at', '=', null)
                    ->order_by('date', 'asc');

        // 本日以降
        if ($period == 'later') {
            $query->where('date', '>=', Date::forge()->format('mysql_date'));
        // 本日より前（本日は含まない）
        } elseif ($period == 'past') {
            $query->where('date', '<', Date::forge()->format('mysql_date'));
        }

        return $query->execute()->as_array();
    }

    /**
     * 指定した日程の出欠を、メンバー情報を含めて取得
     * @param int $schedule_id
     */
    public static function getListIncludeMemberInfo($schedule_id)
    {
        $current_year =Service_Year::getCurrentYear();
        $columns = [
            'join_year.member_id', 'section_id', 'part_id', 'last_name', 'first_name', 'attendance_id'
        ];
        return DB::select_array($columns)->from('join_year')
                ->join('member', 'left')->on('join_year.member_id', '=', 'member.id')
                ->join(static::$table, 'left')
                    ->on('join_year.member_id', '=', static::$table.'.member_id')
                    ->on(static::$table.'.schedule_id', '=', DB::expr($schedule_id))
                ->where('join_year', '=', $current_year)
                ->order_by(DB::expr('attendance_id is null'))
                ->order_by('attendance_id', 'asc')
                ->order_by('section_id', 'asc')
                ->order_by('part_id', 'asc')
                ->execute()->as_array('member_id');
    }

    /**
     * 指定年度の出欠数を取得
     * @param int $year
     * @return array
     */
    public static function getAttendanceCount($year)
    {
        $result = DB::select('a.member_id', 'a.attendance_id', [DB::expr('COUNT("a.attendance_id")'), 'attendance_count'])
                    ->from(['attendance', 'a'])
                    ->join(['schedule', 's'], 'left')->on('a.schedule_id', '=', 's.id')
                    ->where(['s.year' => $year, 'count_check' => true])
                    ->where('s.deleted_at', 'is', null)
                    ->group_by('a.attendance_id', 'a.member_id')
                    ->order_by('a.member_id', 'asc')
                    ->order_by('a.attendance_id', 'asc')
                    ->execute()->as_array();

        $attendance_count_list = [];
        foreach ($result as $key => $value) {
            $attendance_count_list[$value['member_id']][$value['attendance_id']] = $value;
            $attendance_count_list[$value['member_id']]['total'] = isset($attendance_count_list[$value['member_id']]['total'])
                                                                    ? $attendance_count_list[$value['member_id']]['total'] + $value['attendance_count']
                                                                    : $value['attendance_count'];
        }

        return $attendance_count_list;
    }
}
