<?php

class Model_Member extends Model_La
{
    protected static $table = 'member';

    /**
     * ログインID(メールアドレス)の登録重複チェック
     * @return bool
     */
    public static function checkDuplicateLoginId($login_id)
    {
        $result = DB::select('login_id')->from('member')
                    ->where('login_id', '=', $login_id)
                    ->execute()->as_array();
        $cnt = count($result);

        if ($cnt > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * メンバーのフルネーム一覧を取得
     * @return array
     */
    public static function getNameList($ids = [])
    {
        $query = DB::select('id', DB::expr('CONCAT(`last_name`, " ", `first_name`) as name'), DB::expr('CONCAT(`last_name_kana`, " ", `first_name_kana`) as kana'))
                    ->from('member')
                    ->where('id', '>=', 10);

        if (count($ids) >= 1) {
            $query->where('id', 'in', $ids);
        }

        $result = $query->order_by('kana', 'asc')
                    ->cached(86400)->execute()->as_array('id');

        $name_list = [];
        foreach ($result as $key => $value) {
            $name_list[$key] = $value['name'];
        }

        return $name_list;
    }

    public static function isDeveloper($id)
    {
        $result = DB::select('developer')->from(static::$table)->where('id', '=', $id)->execute()->current();
        return $result['developer'];
    }
}