<?php

class Model_Position extends Model_La
{
    protected static $table = 'position';

    /**
     * 該当年度の各ポジションの人数を取得
     *
     * @param int $join_year 年度
     * @return array
     */
    public static function getPositionCount($join_year)
    {
        $result = DB::select('position_id')->from('position')
                    ->where('join_year', '=', $join_year)
                    ->execute()->as_array();

        $position_count = [];
        foreach ($result as $value) {
            $position_ids = json_decode($value['position_id']);
            foreach ($position_ids as $position_id) {
                if (!isset($position_count[$position_id])) {
                    $position_count[$position_id] = 0;
                }
                $position_count[$position_id]++;
            }
        }

        return $position_count;
    }

    /**
     * 参加年度のポジションをセット
     * @param int $profile   プロフィール
     * @return void
     */
    public static function setPosition($profile)
    {
        $position_ids = (is_array($profile['position_id'])) ? $profile['position_id'] : [$profile['position_id']];
        $set_list = [
                'member_id'      => $profile['member_id'],
                'join_year'      => $profile['join_year'],
                'position_id'    => json_encode($position_ids),
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => '0000-00-00 00:00:00',
            ];

        DB::insert('position')->set($set_list)->execute();
    }
}