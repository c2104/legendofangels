<?php

class Model_Schedule extends Model_La
{
    protected static $table = 'schedule';

    /**
     * スケジュール一覧を取得
     * @param string $period 表示する期間
     *      'all'   => 全期間
     *      'later' => 本日以降
     *      'past'  => 本日より前（本日は含まない）
     * @param int $year 年度
     * @return array
     */
    public static function getSchedules($period = 'all', $year = null, $count_check = false)
    {
        // 年度の指定がない場合は今期を設定
        if (!$year) {
            $year = Service_Year::getCurrentYear();
        }

        $query = DB::select()->from(static::$table)
                    ->where('year', '=', $year)
                    ->where('deleted_at', '=', null)
                    ->order_by('date', 'asc');

        // 本日以降
        if ($period == 'later') {
            $query->where('date', '>=', Date::forge()->format('mysql_date'));
        // 本日より前（本日は含まない）
        } elseif ($period == 'past') {
            $query->where('date', '<', Date::forge()->format('mysql_date'));
        }

        // 出欠を数えるスケジュールのみ取得
        if ($count_check) {
            $query->where('count_check', '=', true);
        }

        return $query->execute()->as_array('id');
    }

    /**
     * 当日を含む次のスケジュールを取得
     * @return array
     */
    public static function getNextSchedule()
    {
        $result =  DB::select()->from(static::$table)
                    ->where('date', '>=', date('Y-m-d'))
                    ->where('deleted_at', '=', null)
                    ->order_by('date', 'asc')
                    ->execute()->current();

        if (!$result) {
            return [];
        }
        return $result;
    }

    /**
     * 初回スケジュールを取得
     * @param int $year
     * @return array
     */
    public static function getFirstSchedule($year = null)
    {
        // 年度の指定がない場合は今期を設定
        if (!$year) {
            $year =Service_Year::getCurrentYear();
        }

        return DB::select()->from(static::$table)
                ->where('year', '=', $year)
                ->where('deleted_at', '=', null)
                ->order_by('date', 'asc')
                ->execute()->current();
    }

    /**
     * 日程の登録重複チェック
     * @return bool
     */
    public static function checkDuplicateSchedule($date)
    {
        $result = DB::select('date')->from(static::$table)
                    ->where('date', '=', $date)
                    ->where('deleted_at', '=', null)
                    ->execute()->current();

        return (count($result) > 0) ? false : true;
    }

    public static function setSchedule($data)
    {
        $set_list = [
            'year'        => $data['year'],
            'date'        => $data['date'],
            'count_check' => $data['count_check'] ? true : false,
            'event'       => $data['event'],
            'created_at'  => date('Y-m-d H:i:s'),
            'updated_at'  => '0000-00-00 00:00:00',
        ];

        DB::insert(static::$table)->set($set_list)->execute();
    }
}
