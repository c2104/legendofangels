<?php

class Model_JoinYear extends Model_La
{
    protected static $table = 'join_year';

    /**
     * 該当年度のメンバー一覧を取得
     *
     * @param int $join_year 年度
     * @return array
     */
    public static function getMemberListBySelectyear($select_year)
    {
        // メンバー一覧取得
        $sel = [['join.member_id', 'id'], 'last_name', 'first_name', 'last_name_kana', 'first_name_kana',
                'developer', 'join.join_year', 'section_id', 'part_id'];

        $member_list = DB::select_array($sel)->from(['join_year', 'join'])
                        ->join('member', 'left')->on('join.member_id', '=', 'member.id')
                        ->where('last_name', '!=', 'テスト')
                        ->where('join.join_year', '=', $select_year)
                        ->order_by('section_id', 'asc')
                        ->order_by('part_id', 'asc')
                        ->execute()->as_array();

        return $member_list;
    }

    /**
     * 該当年度の各メンバーのポジションリストを取得
     *
     * @param int $join_year 年度
     * @return array
     */
    public static function getMemberPositionList($select_year)
    {
        // メンバー一覧取得
        $sel = [['join.member_id', 'id'], 'last_name', 'first_name', 'last_name_kana', 'first_name_kana',
                'developer', 'join.join_year', 'position_id', 'section_id', 'part_id'];

        $member_list = DB::select_array($sel)->from(['join_year', 'join'])
                        ->join('member', 'left')->on('join.member_id', '=', 'member.id')
                        ->join('position', 'left')->on('join.member_id', '=', 'position.member_id')
                                                  ->on('join.join_year', '=', 'position.join_year')
                        ->where('last_name', '!=', 'テスト')
                        ->where('join.join_year', '=', $select_year)
                        ->order_by('section_id', 'asc')
                        ->order_by('part_id', 'asc')
                        ->execute()->as_array('id');

        foreach ($member_list as $key => $member) {
            $member_list[$key]['position_id'] = json_decode($member['position_id']);
        }

        return $member_list;
    }

    /**
     * メンバーIDを元に、参加年度を取得
     *
     * @param int $member_id メンバーID
     * @return array
     */
    public static function getJoinYear($member_id)
    {
        $join_year =  DB::select('join.*', 'position_id')->from(['join_year', 'join'])
                        ->join('position', 'left')->on('join.member_id', '=', 'position.member_id')
                                                ->on('join.join_year', '=', 'position.join_year')
                        ->where('join.member_id', '=', $member_id)
                        ->order_by('join.join_year', 'desc')
                        ->execute()->as_array();

        foreach ($join_year as $key => $member) {
            $join_year[$key]['position_id'] = json_decode($member['position_id']);
        }

        return $join_year;
    }

    /**
     * 本年度の自身のポジションを取得
     *
     * @param int $member_id メンバーID
     * @return array
     */
    public static function getCurrentYearPosition($member_id)
    {
        $current_year =Service_Year::getCurrentYear();

        $result =  DB::select('join_year.*', 'position_id', 'login_hash')->from('join_year')
                    ->join('member', 'left')->on('member.id', '=', 'join_year.member_id')
                    ->join('position', 'left')->on('join_year.member_id', '=', 'position.member_id')
                                              ->on('join_year.join_year', '=', 'position.join_year')
                    ->where('join_year.member_id', '=', $member_id)
                    ->where('join_year.join_year', '=', $current_year)
                    ->execute()->current();

        if ($result) {
            $result['position_id'] = json_decode($result['position_id'], true);
        }

        return $result;
    }

    /**
     * 参加年度の登録重複チェック
     * @return bool
     */
    public static function checkDuplicateJoinYear($member_id, $join_year)
    {
        $result = DB::select('join_year')->from('join_year')
                    ->where('member_id', '=', $member_id)
                    ->where('join_year', '=', $join_year)
                    ->execute()->current();

        if (count($result) > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 参加年度をセット
     * @param int $profile   プロフィール
     * @return void
     */
    public static function setJoinYear($profile)
    {
        $set_list = [
                'join_year'      => $profile['join_year'],
                'member_id'      => $profile['member_id'],
                'section_id'     => $profile['section_id'],
                'part_id'        => $profile['part_id'],
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => '0000-00-00 00:00:00',
            ];

        DB::insert('join_year')->set($set_list)->execute();
    }
}
