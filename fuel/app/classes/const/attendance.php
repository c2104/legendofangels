<?php

/*
 * 出欠用定数クラス
 */
class Const_Attendance
{
    const ATTEND     = 1;
    const ABSENCE    = 2;
    const LATE       = 3;
    const LEAVE_EALY = 4;
    const UNDECIDED  = 9;
    const NONE       = 10;

    const PARAMETER = [
        1  => '出席',
        2  => '欠席',
        3  => '遅刻',
        4  => '早退',
        9  => '未定',
        10 => '-',
    ];
}
