<?php
/**
 * URI関連
 */
class LaUri
{
    /**
     * ベースURL取得
     */
    public static function baseUrl()
    {
        return Config::get('top_url');
    }

    /**
     * ログイン後トップURL取得
     */
    public static function topUrl()
    {
        return Config::get('top_url');
    }
}
