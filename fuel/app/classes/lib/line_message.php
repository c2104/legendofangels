<?php
/**
 * LineMessage Line Message API用関数
 */
class LineMessage
{
    /**
     * ユーザーIDを確認
     */
    public static function checkUserId($user_id)
    {
        $user = Model_LineApi::current(['user_id' => $user_id]);
        return $user ?? false;
    }

    /**
     * レスポンスデータを作成
     */
    public static function setResponse($messages)
    {
        if (is_array($messages)) {
            foreach ($messages as $key => $message) {
                $result[] = [
                    'type' => 'text',
                    'text' => $message
                ];
            }
        } else {
            $result[] = [
                'type' => 'text',
                'text' => $messages
            ];
        }

        return $result;
    }
}
