<?php
/**
 * Commonfunc 共通関数
 * @package  app
 * @extends  Controller
 */
class Commonfunc
{
    /**
     * ログイン後トップURL取得
     */
    public static function getTopUrl()
    {
        return Config::get('top_url');
    }

    /**
     * 配列の先頭に"▼選択してください"を追加
     *
     * @return array
     */
    public static function insertPleaseSelectToArray($arr)
    {
        $init = ['' => ['id' => '', 'name' => '▼ 選択してください', 'tag' => '']];
        $merged_arr = array_merge($init, $arr);

        return $merged_arr;
    }

    /**
     * 曜日を取得
     * @param string $date
     */
    public static function getDayofweek($date)
    {
        Config::load('commoncfg', true);

        $dow    = Config::get('commoncfg.dow');
        $dow_id = date('w', strtotime($date));

        return $dow[$dow_id];
    }
}
