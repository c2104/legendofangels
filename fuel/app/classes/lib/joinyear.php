<?php
/**
 * JoinYear 年度関数
 * @package  app
 * @extends  Controller
 */
class JoinYear
{
    /**
     * 該当の年度に参加しているか
     * @param  int   $member_id メンバーID
     * @param  int   $year  参加年度
     * @return bool
     */
    public static function isJoinedYear($member_id, $year = null)
    {
        if (!$year) {
            $year = (int)self::getCurrentYear();
        }
        $result = Model_Joinyear::count(['member_id' => $member_id, 'join_year' => $year]);

        if ($result >= 1) {
            return true;
        } else {
            return false;
        }
    }
}
