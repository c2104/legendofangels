<?php
return array(
  'version' => 
  array(
    'app' => 
    array(
      'default' => 
      array(
        0 => '001_create_member',
        1 => '002_add_last_login_to_member',
        2 => '003_add_login_hash_to_member',
        3 => '004_create_position',
        4 => '005_create_section',
        5 => '006_create_part',
        6 => '007_add_section_id_to_part',
        7 => '008_created_at_and_updated_at_and_last_login_arrange_to_member',
        8 => '009_add_last_name_kana_and_first_name_kana_to_member',
        9 => '010_create_join_year',
        10 => '011_add_developer_to_member',
        11 => '012_add_positions_to_join_year',
        12 => '013_create_schedule',
        13 => '014_create_attendance',
        14 => '015_add_hash_to_member',
        15 => '016_create_infomation',
        16 => '017_add_ismember_to_position',
        17 => '018_change_positionid_to_position',
        18 => '019_add_deletedat_to_schedule',
        19 => '020_create_line_api',
      ),
    ),
    'module' => 
    array(
    ),
    'package' => 
    array(
    ),
  ),
  'folder' => 'migrations/',
  'table' => 'migration',
  'flush_cache' => false,
);
