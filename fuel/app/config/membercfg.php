<?php
/**
 * membercfg.php
 * メンバーページ システム設定ファイル
 */
return [
    'position' => [
        'member'         => ['id' => 1,  'name' => 'メンバー', 'tag' => 'member'],
        'part_leader'    => ['id' => 2,  'name' => 'パートリーダー', 'tag' => 'part_leader'],
        'section_leader' => ['id' => 3,  'name' => 'セクションリーダー', 'tag' => 'section_leader'],
        'create'         => ['id' => 4,  'name' => 'クリエイト', 'tag' => 'create'],
        'manage'         => ['id' => 5,  'name' => 'マネージメント', 'tag' => 'manage'],
        'support'        => ['id' => 6,  'name' => 'サポート', 'tag' => 'support'],
        'instructor'     => ['id' => 7,  'name' => 'インストラクター', 'tag' => 'instructor'],
        'tech'           => ['id' => 8,  'name' => 'テクニカル', 'tag' => 'tech'],
        'teacher'        => ['id' => 9,  'name' => '講師', 'tag' => 'teacher'],
        'director'       => ['id' => 10, 'name' => 'ディレクター', 'tag' => 'director'],
        'designer'       => ['id' => 11, 'name' => 'ショーデザイナー', 'tag' => 'designer'],
    ],
    'section' => [
        'horn'  => ['id' => 1,  'name' => 'ホーン', 'short' => 'Horn'],
        'batt'  => ['id' => 2,  'name' => 'バッテリー', 'short' => 'Batt'],
        'pit'   => ['id' => 3,  'name' => 'ピット', 'short' => 'Pit'],
        'cg'    => ['id' => 4,  'name' => 'カラーガード', 'short' => 'CG'],
        'dm'    => ['id' => 5,  'name' => 'ドラムメジャー', 'short' => 'DM'],
    ],
    'part' => [
        'sop'    => ['id' => 1,  'name' => 'ソプラノ', 'short' => 'Sop'],
        'mello'  => ['id' => 2,  'name' => 'メロフォン', 'short' => 'Mello'],
        'bari'   => ['id' => 3,  'name' => 'バリトン', 'short' => 'Bari'],
        'euph'   => ['id' => 4,  'name' => 'ユーフォニアム', 'short' => 'Euph'],
        'contra' => ['id' => 5,  'name' => 'コントラバス', 'short' => 'Cb'],
        'snare'  => ['id' => 6,  'name' => 'スネア', 'short' => 'SD'],
        'tenor'  => ['id' => 7,  'name' => 'テナー', 'short' => 'TD'],
        'bass'   => ['id' => 8,  'name' => 'ベース', 'short' => 'BD'],
        'pit'    => ['id' => 9,  'name' => 'ピット', 'short' => 'Pit'],
        'cg'     => ['id' => 10, 'name' => 'カラーガード', 'short' => 'CG'],
        'dm'     => ['id' => 11, 'name' => 'ドラムメジャー', 'short' => 'DM'],
    ],
    'part_map' => [
        1 => [1  => null, 2 => null, 3 => null, 4 => null, 5 => null],
        2 => [6  => null, 7 => null, 8 => null],
        3 => [9  => null],
        4 => [10 => null],
        5 => [11 => null],
    ],

    // 出欠
    'attendance' => [
        1  => '出席',
        2  => '欠席',
        3  => '遅刻',
        4  => '早退',
        9  => '未定',
        10 => '-',
    ],
];
