<?php
/**
 * The production database settings. These get merged with the global settings.
 */

$host     = 'legend-of-angels.org';
$dbname   = 'legend-of-angels-admin';
$username = 'developer';
$password = 'colorofoceans2002';

return array(
    'default' => array(
        'type'       => 'pdo',
        'connection' => array(
            'dsn'        => 'mysql:host='.$host.';dbname='.$dbname.';unix_socket=/var/lib/mysql/mysql.sock',
            'username'   => $username,
            'password'   => $password,
            'persistent' => false,
            'compress'   => false,
        ),
        'table_prefix' => '',
        'charset'      => 'utf8',
        'enable_cache' => true,
        'profiling'    => false,
    ),
);
