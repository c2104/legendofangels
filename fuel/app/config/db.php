<?php
/**
 * Use this file to override global defaults.
 *
 * See the individual environment DB configs for specific config information.
 */

return array(
    'default' => array(
        'connection'  => array(
            'dsn'        => 'mysql:host=localhost;dbname=legend-of-angels-admin',
            'username'   => 'root',
            'password'   => 'sjq90554',
        ),
        'profiling' => true,
    ),
);
