<?php

namespace Fuel\Migrations;

class Add_section_id_to_part
{
	public function up()
	{
		\DBUtil::add_fields('part', array(
			'section_id' => array(
				'constraint' => 11,
				'type' => 'int',
                'comment' => 'セクションID',
                'after' => 'id'
			),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('part', array(
			'section_id'

		));
	}
}