<?php

namespace Fuel\Migrations;

class Create_attendance
{
    public function up()
    {
        \DBUtil::create_table('attendance', [
            'id'            => ['constraint'     => 11,
                                'type'           => 'int',
                                'auto_increment' => true,
                                'unsigned'       => true,
                                'comment'        => '出席ID'
                ],
            'member_id'     => ['constraint'    => 11,
                                'type'          => 'int',
                                'comment'       => 'メンバーID'
                ],
            'schedule_id'   => ['constraint'    => 11,
                                'type'          => 'int',
                                'comment'       => 'スケジュールID'
                ],
            'attendance_id'    => ['constraint' => 11,
                                'type'       => 'int',
                                'comment'    => '出欠'
                ],
            'created_at' => [   'type'    => 'datetime',
                                'comment' => '作成日'
                ],
            'updated_at' => [   'type'    => 'datetime',
                                'comment' => '更新日'
                ],
        ], ['id']);

        \DBUtil::create_index('attendance', 'member_id');
        \DBUtil::create_index('attendance', 'schedule_id');
        \DBUtil::create_index('attendance', 'attendance_id');
    }

    public function down()
    {
        \DBUtil::drop_table('attendance');
    }
}