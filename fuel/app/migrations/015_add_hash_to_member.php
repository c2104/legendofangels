<?php

namespace Fuel\Migrations;

class Add_hash_to_member
{
	public function up()
	{
		\DBUtil::add_fields('member', array(
			'hash' => array(
				'constraint' => 255,
				'type' => 'varchar',
                'null' => true,
                'comment' => '個人ハッシュ',
                'after' => 'developer'
			),

		));

		\DBUtil::create_index('member', 'hash');

		$members = \DB::select()->from('member')->execute()->as_array();
		foreach ($members as $member) {
			$hash = \Str::random('alnum', 40);
			$member_hash = \DB::select('hash')->from('member')->where('hash', '!=', null)->execute()->as_array();
			do {
				if (in_array($hash, $member_hash)) {
					$hash = \Str::random('alnum', 40);
				} else {
					\DB::update('member')->set(['hash' => $hash])->where('id', $member['id'])->execute();
					break;
				}
			} while (0);
		}
	}

	public function down()
	{
		\DBUtil::drop_fields('member', array(
			'hash'

		));
	}
}