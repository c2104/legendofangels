<?php

namespace Fuel\Migrations;

class Create_position
{
	public function up()
	{
		\DBUtil::create_table('position', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'null' => true, 'auto_increment' => true, 'unsigned' => true, 'comment' => 'ポジションID'),
			'name' => array('constraint' => 100, 'type' => 'varchar', 'comment' => 'ポジション'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('position');
	}
}