<?php

namespace Fuel\Migrations;

class Add_ismember_to_position
{
    public function up()
    {
        \DBUtil::add_fields('position', [
            'is_member' => [
                'constraint' => 4,
                'type' => 'tinyint',
                'comment' => 'メンバーかどうか',
                'after' => 'position_id'
            ],
        ]);

        \DB::update('position')->set(['is_member' => true])->execute();
    }

    public function down()
    {
        \DBUtil::drop_fields('position', [
            'is_member'
        ]);
    }
}