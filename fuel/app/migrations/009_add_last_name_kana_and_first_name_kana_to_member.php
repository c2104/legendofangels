<?php

namespace Fuel\Migrations;

class Add_last_name_kana_and_first_name_kana_to_member
{
	public function up()
	{
		\DBUtil::add_fields('member', array(
			'last_name_kana' => array('constraint' => 255, 'type' => 'varchar', 'comment' => '姓かな', 'after' => 'first_name'),
			'first_name_kana' => array('constraint' => 255, 'type' => 'varchar', 'comment' => '名かな', 'after' => 'last_name_kana'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('member', array(
			'last_name_kana',
			'first_name_kana',

		));
	}
}