<?php

namespace Fuel\Migrations;

class Create_section
{
	public function up()
	{
		\DBUtil::create_table('section', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'null' => true, 'auto_increment' => true, 'unsigned' => true, 'comment' => 'セクションID'),
			'name' => array('constraint' => 100, 'type' => 'varchar', 'comment' => 'セクション'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('section');
	}
}