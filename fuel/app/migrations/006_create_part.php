<?php

namespace Fuel\Migrations;

class Create_part
{
	public function up()
	{
		\DBUtil::create_table('part', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'null' => true, 'auto_increment' => true, 'unsigned' => true, 'comment' => 'パートID'),
			'name' => array('constraint' => 100, 'type' => 'varchar', 'comment' => 'パート'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('part');
	}
}