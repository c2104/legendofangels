<?php

namespace Fuel\Migrations;

class Create_line_api
{
	public function up()
	{
        \DBUtil::create_table('line_api', [
            'id'         => [   'constraint'     => 11,
                                'type'           => 'int',
                                'auto_increment' => true,
                                'unsigned'       => true,
                                'comment'        => 'ID'
                ],
            'member_id'  => [   'constraint'    => 11,
                                'type'          => 'int',
                                'comment'       => 'メンバーID'
                ],
            'user_id' => [   'constraint' => 100,
                                'type'       => 'varchar',
                                'null'       => true,
                                'comment'    => 'LINEユーザーID',
                ],
            'created_at' => [   'type'    => 'datetime',
                                'comment' => '作成日'
                ],
        ], ['id']);

        \DBUtil::create_index('line_api', 'member_id');
	}

	public function down()
	{
		\DBUtil::drop_table('line_api');
	}
}