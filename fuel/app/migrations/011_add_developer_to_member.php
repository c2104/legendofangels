<?php

namespace Fuel\Migrations;

class Add_developer_to_member
{
	public function up()
	{
		\DBUtil::add_fields('member', array(
			'developer' => array('type' => 'tinyint', 'comment' => '管理者権限', 'after' => 'first_name_kana'),
		));
	}

	public function down()
	{
		\DBUtil::drop_fields('member', array(
			'developer',
		));
	}
}