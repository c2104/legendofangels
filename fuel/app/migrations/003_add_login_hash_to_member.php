<?php

namespace Fuel\Migrations;

class Add_login_hash_to_member
{
	public function up()
	{
		\DBUtil::add_fields('member', array(
			'login_hash' => array(
				'constraint' => 255,
				'type' => 'varchar',
                'null' => true,
                'comment' => 'ログインハッシュ',
                'after' => 'last_login'
			),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('member', array(
			'login_hash'

		));
	}
}