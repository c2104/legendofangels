<?php

namespace Fuel\Migrations;

class Create_schedule
{
    public function up()
    {
        \DBUtil::create_table('schedule', [
            'id'          => [  'constraint'     => 11,
                                'type'           => 'int',
                                'null'           => true,
                                'auto_increment' => true,
                                'unsigned'       => true,
                                'comment'        => 'スケジュールID'
                ],
            'dow'         => [  'constraint' => 10,
                                'type'       => 'varchar',
                                'comment'    => '曜日（day of the week）'
                ],
            'year' => [  'constraint' => 11,
                                'type'       => 'int',
                                'comment'    => '年度'
                ],
            'date'        => [  'type'    => 'date',
                                'comment' => '日程'
                ],
            'event'       => [  'constraint' => 200,
                                'type'       => 'varchar',
                                'comment'    => 'イベント'
                ],
            'created_at' => [   'type'    => 'datetime',
                                'comment' => '作成日'
                ],
            'updated_at' => [   'type'    => 'datetime',
                                'comment' => '更新日'
                ],
        ], ['id']);
    }

    public function down()
    {
        \DBUtil::drop_table('schedule');
    }
}