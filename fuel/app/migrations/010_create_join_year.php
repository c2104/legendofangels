<?php

namespace Fuel\Migrations;

class Create_join_year
{
	public function up()
	{
		\DBUtil::create_table('join_year', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'null' => true, 'auto_increment' => true, 'unsigned' => true, 'comment' => '参加年度ID'),
			'join_year' => array('constraint' => 11, 'type' => 'int', 'comment' => '参加年度'),
			'member_id' => array('constraint' => 11, 'type' => 'int', 'comment' => 'メンバーID'),
			'section_id' => array('constraint' => 11, 'type' => 'int', 'comment' => 'セクションID'),
			'part_id' => array('constraint' => 11, 'type' => 'int', 'comment' => 'パートID'),
			'created_at' => array('type' => 'datetime', 'comment' => '作成日'),
			'updated_at' => array('type' => 'datetime', 'comment' => '更新日'),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('join_year');
	}
}