<?php

namespace Fuel\Migrations;

class Create_infomation
{
	public function up()
	{
        \DBUtil::create_table('infomation', [
            'id'         => [   'constraint'     => 11,
                                'type'           => 'int',
                                'auto_increment' => true,
                                'unsigned'       => true,
                                'comment'        => '出席ID'
                ],
            'member_id'  => [   'constraint'    => 11,
                                'type'          => 'int',
                                'comment'       => 'メンバーID'
                ],
            'year'       => [   'constraint'    => 11,
                                'type'          => 'int',
                                'comment'       => '年度'
                ],
            'infomation' => [   'constraint' => 500,
                                'type'       => 'varchar',
                                'null'       => true,
                                'comment'    => '配信情報',
                ],
            'created_at' => [   'type'    => 'datetime',
                                'comment' => '作成日'
                ],
        ], ['id']);

        \DBUtil::create_index('infomation', 'member_id');
	}

	public function down()
	{
		\DBUtil::drop_table('infomation');
	}
}