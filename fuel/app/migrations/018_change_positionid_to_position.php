<?php

namespace Fuel\Migrations;

class Change_positionid_to_position
{
    public function up()
    {
        $positions = \DB::select('id', 'position_id')->from('position')->execute()->as_array();
        \DBUtil::modify_fields('position', [
            'position_id' => [
                'type' => 'varchar',
                'constraint' => 255,
                'comment' => 'ポジションID（JSON）'
            ],
        ]);
        foreach ($positions as $key => $value) {
            $update = ['position_id' => json_encode([$value['position_id']])];
            \Model_Position::update($update, ['id' => $value['id']]);
        }

        \DBUtil::drop_fields('position', 'is_member');
    }

    public function down()
    {
        $positions = \DB::select('id', 'position_id')->from('position')->execute()->as_array();
        \DBUtil::modify_fields('position', [
            'position_id' => [
                'type' => 'int',
                'constraint' => 11,
                'comment' => 'ポジションID'
            ],
        ]);
        foreach ($positions as $key => $value) {
            $update = ['position_id' => json_decode([$value['position_id']])];
            \Model_Position::update($update, ['id' => $value['id']]);
        }

        \DBUtil::add_fields('position', [
            'is_member' => [
                'constraint' => 4,
                'type' => 'tinyint',
                'comment' => 'メンバーかどうか',
                'after' => 'position_id'
            ],
        ]);

        \DB::update('position')->set(['is_member' => true])->execute();
    }
}