<?php

namespace Fuel\Migrations;

class Add_last_login_to_member
{
	public function up()
	{
		\DBUtil::add_fields('member', array(
			'last_login' => array(
				'constraint' => 11,
				'type' => 'int',
                'null' => true,
                'comment' => '最終ログイン日時',
                'after' => 'part'
			),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('member', array(
			'last_login'

		));
	}
}