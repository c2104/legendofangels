<?php

namespace Fuel\Migrations;

class Add_positions_to_join_year
{
	public function up()
	{
		\DBUtil::add_fields('join_year', array(
			'teacher' => array('type' => 'tinyint', 'comment' => 'メンバー', 'after' => 'part_id'),
			'tech' => array('type' => 'tinyint', 'comment' => 'メンバー', 'after' => 'part_id'),
			'instructor' => array('type' => 'tinyint', 'comment' => 'メンバー', 'after' => 'part_id'),
			'support' => array('type' => 'tinyint', 'comment' => 'メンバー', 'after' => 'part_id'),
			'manage' => array('type' => 'tinyint', 'comment' => 'メンバー', 'after' => 'part_id'),
			'create' => array('type' => 'tinyint', 'comment' => 'メンバー', 'after' => 'part_id'),
			'section_leader' => array('type' => 'tinyint', 'comment' => 'メンバー', 'after' => 'part_id'),
			'part_leader' => array('type' => 'tinyint', 'comment' => 'メンバー', 'after' => 'part_id'),
			'member' => array('type' => 'tinyint', 'comment' => 'メンバー', 'after' => 'part_id'),
		));
	}

	public function down()
	{
		\DBUtil::drop_fields('join_year', array(
			'member',
			'section_leader',
			'part_leader',
			'create',
			'manage',
			'teacher',
			'instructor',
			'tech',
			'support',
		));
	}
}