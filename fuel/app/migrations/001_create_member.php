<?php

namespace Fuel\Migrations;

class Create_member
{
	public function up()
	{
		\DBUtil::create_table('member', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'null' => true, 'auto_increment' => true, 'unsigned' => true, 'comment' => 'メンバーID'),
			'login_id' => array('constraint' => 100, 'type' => 'varchar', 'comment' => 'ログインID'),
			'password' => array('constraint' => 50, 'type' => 'varchar', 'comment' => 'パスワード'),
			'last_name' => array('constraint' => 50, 'type' => 'varchar', 'comment' => '姓'),
			'first_name' => array('constraint' => 50, 'type' => 'varchar', 'comment' => '名'),
			'created_at' => array('type' => 'datetime', 'comment' => '作成日'),
			'updated_at' => array('type' => 'datetime', 'comment' => '更新日'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('member');
	}
}