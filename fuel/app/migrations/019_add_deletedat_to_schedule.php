<?php

namespace Fuel\Migrations;

class Add_deletedat_to_schedule
{
    public function up()
    {
        \DBUtil::add_fields('schedule', [
            'deleted_at' => [
                'type' => 'datetime',
                'null' => true,
                'default' => null,
                'comment' => '削除日',
                'after' => 'updated_at'
            ],
        ]);
    }

    public function down()
    {
        \DBUtil::drop_fields('schedule', [
            'deleted_at'
        ]);
    }
}