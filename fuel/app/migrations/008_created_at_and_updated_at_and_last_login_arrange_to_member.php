<?php

namespace Fuel\Migrations;

class Created_at_and_updated_at_and_last_login_arrange_to_member
{
    public function up()
    {
        \DBUtil::modify_fields('member', [
            'last_login' => ['type' => 'datetime', 'null' => true],
            'created_at' => ['type' => 'datetime', 'null' => true],
            'updated_at' => ['type' => 'datetime', 'null' => true],
        ]);
    }

    public function down()
    {
        \DBUtil::modify_fields('member', [
            'last_login' => ['constraint' => 11, 'type' => 'int', 'null' => true],
            'created_at' => ['constraint' => 11, 'type' => 'int', 'null' => true],
            'updated_at' => ['constraint' => 11, 'type' => 'int', 'null' => true],
        ]);
    }
}