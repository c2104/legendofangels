<div class="member_profile">
<?php echo Form::open(['action' => '/addjoinyear', 'method' => 'post', 'class' => 'addmember', 'name' => 'addmember']); ?>
    <table class="table table_add">
        <tr class="control_group">
            <th><?php echo Form::label('登録メンバー', 'member_id'); ?></th>
            <td><?php echo Form::select('member_id', Session::get_flash('member_id'), $name_list); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('年度', 'join_year'); ?></th>
            <td><?php echo Form::select('join_year', Session::get_flash('join_year'), $year_list); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('ポジション', 'position_id'); ?></th>
            <td>
                <?php foreach ($positions as $key => $position): ?>
                    <label>
                        <?php echo Form::checkbox('position_id[]', $position['id'], in_array($position['id'], Session::get_flash('position_id') ?? [])); ?>
                        <?php echo $position['name'] ?>
                    </label>
                <?php endforeach ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('セクション', 'section_id'); ?></th>
            <td><?php echo Form::select('section_id', Session::get_flash('section_id'), $sections); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('パート', 'part_id'); ?></th>
            <td><?php echo Form::select('part_id', Session::get_flash('part_id'), $parts); ?></td>
        </tr>
    </table>
    <?php echo Form::submit('confirm', '確認', ['class' => 'btn btn_save mv20']); ?>
    <?php echo Form::hidden(Config::get('security.csrf_token_key'), Security::fetch_token());?>
<?php echo Form::close(); ?>
</div>
