<div class="schedule-form">
    <h2 class="mv10">登録が完了しました。</h2>
    <p class="member-profile__done--msg"><?php echo $msg ?></p>
    <a href="/"><?php echo Form::button('gototop', 'トップへ', ['class' => 'btn btn_top mh10 mv20']); ?></a>
    <a href="/schedule/add"><?php echo Form::button('addschedule', '続けて登録', ['class' => 'btn btn_next mh10 mv20']); ?></a>
</div>
