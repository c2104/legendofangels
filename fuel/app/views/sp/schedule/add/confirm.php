<div class="schedule-form">
<?php echo Form::open(['action' => '/schedule/add/confirm', 'method' => 'post', 'class' => 'addmember', 'name' => 'addueser']); ?>
    <table class="table table_add">
        <tr class="control_group">
            <th><?php echo Form::label('年度', 'year'); ?></th>
            <td><?php echo $year; ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('日程', 'date'); ?></th>
            <td><?php echo $date; ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('出欠カウント', 'count_check'); ?></th>
            <td><?php echo $count_check_display; ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('詳細', 'event'); ?></th>
            <td><?php echo $event; ?></td>
        </tr>
    </table>
    <?php echo Form::submit('back', '修正', ['class' => 'btn btn_back mh10 mv20']); ?>
    <?php echo Form::submit('registration', '登録', ['class' => 'btn btn_save mh10 mv20']); ?>
<?php echo Form::close(); ?>
</div>
