<div class="schedule-form">
<?php echo Form::open(['action' => LaUri::baseUrl().'/schedule/edit?id='.$schedule_id, 'method' => 'post', 'class' => 'addschedule', 'name' => 'addschedule']); ?>
    <table class="table table_add">
        <tr class="control_group">
            <th><?php echo Form::label('年度', 'year'); ?></th>
            <td><?php echo Session::get_flash('year') ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('日程', 'date'); ?></th>
            <td><?php echo Session::get_flash('date') ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('出欠カウント', 'count_check'); ?><br><span class="note">※ 出欠に数えない場合はチェックを外してください</span></th>
            <td><?php echo Form::checkbox('count_check', 1, Session::get_flash('count_check')); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('詳細', 'event'); ?></th>
            <td><?php echo Form::input('event', Session::get_flash('event')); ?></td>
        </tr>
    </table>
    <div>
        <?php echo Form::submit('save', '保存', ['class' => 'btn btn_save mv20']); ?>
        <?php echo Form::hidden(Config::get('security.csrf_token_key'), Security::fetch_token());?>
    </div>
    <div>
        <?php echo Form::submit('delete', '削除', ['class' => '']); ?>
    </div>
<?php echo Form::close(); ?>
<p class="clearfix"><a href="<?php echo LaUri::baseUrl().'/schedule' ?>" class="pull-left">< 戻る</a></p>
</div>

<?php echo Asset::js('schedule/edit.js') ?>