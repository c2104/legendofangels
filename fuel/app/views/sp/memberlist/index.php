<div class="year_list">
<?php echo Form::open(['action' => '/memberlist', 'id' => 'fiscal_year', 'method' => 'GET']) ?>
    年度：<?php echo Form::select('year', Input::get('year') ,$year_list, ['class' => 'ph20 pv5', 'onchange' => 'this.form.submit()']) ?>
<?php echo Form::close() ?>
</div>
<div class="member_list">
    <?php foreach ($positions as $position): ?>
        <div class="position">
            <table class="table table_list">
                <caption class="position_name">
                    <?php echo $position['name'] ?>
                    <?php echo isset($position_count[$position['id']]) ? $position_count[$position['id']] : 0 ?>名
                </caption>
                <tr>
                    <th class="name">名前</th>
                    <th>セクション</th>
                    <th>パート</th>
                </tr>
                <?php foreach ($member_list as $member): ?>
                    <?php if (in_array($position['id'], $member['position_id'])): ?>
                        <tr>
                            <td class="name">
                                <?php if ($prof_link): ?>
                                    <a href="/profile?id=<?php echo $member['id'] ?>"><?php echo $member['last_name'].' '.$member['first_name'] ?></a><br>
                                <?php else: ?>
                                    <?php echo $member['last_name'].' '.$member['first_name'] ?><br>
                                <?php endif ?>
                                <span class="secondary_text fz11"><?php echo $member['last_name_kana'].' '.$member['first_name_kana'] ?></span>
                            </td>
                            <td>
                                <?php foreach ($sections as $section): ?>
                                    <?php if ($member['section_id'] == $section['id']): ?>
                                        <?php echo $section['name'] ?>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </td>
                            <td>
                                <?php foreach ($parts as $part): ?>
                                    <?php if ($member['part_id'] == $part['id']): ?>
                                        <?php echo $part['name'] ?>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </td>
                        </tr>
                    <?php endif ?>
                <?php endforeach ?>
            </table>
        </div>
    <?php endforeach ?>
</div>
