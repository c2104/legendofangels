<div class="year_list">
    <?php echo Form::open(['action' => '/attendcount', 'id' => 'fiscal_year', 'method' => 'GET']) ?>
        年度：<?php echo Form::select('year', Input::get('year') ,$year_list, ['class' => 'ph20 pv5', 'onchange' => 'this.form.submit()']) ?>
    <?php echo Form::close() ?>
</div>
<div class="member_list">
    <div class="position">
        <table class="table table_list">
            <caption class="position_name">
                出欠を数える日程数<span class="m10 fz25"><?php echo $all_schedule_count; ?></span>日<br>
            </caption>
            <tr>
                <th class="member-list__header--name">名前</th>
                <th>出席率 / 皆勤</th>
            </tr>
            <?php foreach ($attendance_rate_list as $member_id => $attendance_rate): ?>
                <?php if ($member_list[$member_id]['section_id'] == 0) continue; ?>
                <tr>
                    <td class="member-list__body--name">
                        <?php if ($prof_link): ?>
                            <a href="/profile?id=<?php echo $member_list[$member_id]['id'] ?>"><?php echo $member_list[$member_id]['last_name'].' '.$member_list[$member_id]['first_name'] ?></a><br>
                        <?php else: ?>
                            <?php echo $member_list[$member_id]['last_name'].' '.$member_list[$member_id]['first_name'] ?><br>
                        <?php endif ?>
                        <p class="secondary_text fz11"><?php echo $member_list[$member_id]['last_name_kana'].' '.$member_list[$member_id]['first_name_kana'] ?></p>
                        <?php if (isset($attendance_count_list[$member_id])): ?>
                            <?php $member_attendance = $attendance_count_list[$member_id]; ?>
                            <p class="mb5 <?php echo ($all_schedule_count != $member_attendance['total']) ? 'cf00 bold' : '' ?>">出欠登録数 <?php echo $member_attendance['total'] ?></p>
                            <p>出席：<?php echo isset($member_attendance[1]) ? $member_attendance[1]['attendance_count'] : 0; ?></p>
                            <p>欠席：<?php echo isset($member_attendance[2]) ? $member_attendance[2]['attendance_count'] : 0; ?></p>
                            <p>遅刻：<?php echo isset($member_attendance[3]) ? $member_attendance[3]['attendance_count'] : 0; ?></p>
                            <p>早退：<?php echo isset($member_attendance[4]) ? $member_attendance[4]['attendance_count'] : 0; ?></p>
                            <p>未定：<?php echo isset($member_attendance[9]) ? $member_attendance[9]['attendance_count'] : 0; ?></p>
                            <p>-：<?php echo isset($member_attendance[10]) ? $member_attendance[10]['attendance_count'] : 0; ?></p>
                        <?php endif ?>
                    </td>
                    <td>
                        <p>「出席」のみ<span class="m10 fz25"><?php echo $attendance_rate['only_attend'] ?></span>%</p>
                        <p> 「出席 遅刻 早退」を含む<span class="m10"><?php echo $attendance_rate['full_attend'] ?></span>%</p>
                        <p class="mb10"><?php echo $attendance_rate['perfect_attendance'] ? '<i class="far fa-star fa-2x m10 gold"></i>' : '' ?></p>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
</div>
