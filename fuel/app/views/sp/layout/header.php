<header>
    <div class="head_area01 navbar navbar-default">
        <div class="navbar-header">
            <?php // バーガーボタン ?>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#gnavi">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="nav__btn--title">メニュー</span>
            </button>

            <a href="/" class="band_name navbar-brand"><?php echo Config::get('band_name'); ?></a>
        </div>

        <div id="gnavi" class="collapse navbar-collapse">
            <ul id="menu_top_nav" class="nav navbar-nav navbar-right">
                <?php if (Auth::check()): ?>
                    <?php if (!Service_Login::isFirstLogin(Auth::get('last_login'))): ?>
                        <li>
                            <a href="/">トップ</a>
                        </li>
                        <li>
                            <a href="/memberlist"><span class="txt_hover">メンバー一覧</span></a>
                        </li>
                        <li class="widget_menu"><a href="/attendlist"><span class="txt_hover">出欠一覧</span></a></li>

                        <?php // グローバルナビ ?>
                        <li>
                            <div class="config dropdown">
                                <button id="config--dropdown" class="dropdown-toggle" type="button" data-toggle="dropdown">
                                    設定<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="config--dropdown">
                                    <li class="dropdown-menu--child dropdown-menu--profile">
                                        <a href="/profile">プロフィール編集</a>
                                    </li>
                                    <li class="dropdown-menu--child dropdown-menu--pass">
                                        <a href="/changepass">パスワード変更</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    <?php endif ?>
                    <li><a href="/logout" id="logout">ログアウト</a></li>
                <?php else: ?>
                    <li><a href="/login" id="official">ログイン</a></li>
                <?php endif ?>
                <li><a href="<?php echo Config::get('official_top_url'); ?>" target="_blank" class="official_link">公式HP <i class="fas fa-external-link-alt fa-fw"></i></a></li>
            </ul>
        </div>
    </div>
</header>
<script type="text/javascript">
$(function() {
    $('#logout').on('click', function() {
        var res = confirm('ログアウトしますか？');
        if (!res) {
            return false;
        }
    });
});
</script>