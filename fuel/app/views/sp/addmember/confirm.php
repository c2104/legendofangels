<div class="member_profile">
<?php echo Form::open(['action' => '/addmember/confirm', 'method' => 'post', 'class' => 'addmember', 'name' => 'addueser']); ?>
    <table class="table table_add">
        <tr class="control_group">
            <th><?php echo Form::label('ログインID', 'login_id'); ?></th>
            <td>
                <?php echo $login_id; ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('名前'); ?></th>
            <td>
                <?php echo Form::label('姓', 'last_name'); ?>
                <?php echo $last_name; ?>
                <?php echo Form::label('名', 'first_name'); ?>
                <?php echo $first_name; ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('かな'); ?></th>
            <td>
                <?php echo Form::label('姓', 'last_name_kana'); ?>
                <?php echo $last_name_kana; ?>
                <?php echo Form::label('名', 'first_name_kana'); ?>
                <?php echo $first_name_kana; ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('年度', 'join_year'); ?></th>
            <td>
                <?php echo $join_year; ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('ポジション', 'position'); ?></th>
            <td>
                <?php echo $position_name; ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('セクション', 'section'); ?></th>
            <td>
                <?php echo $section_name; ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('パート', 'part'); ?></th>
            <td>
                <?php echo $part_name; ?>
            </td>
        </tr>
    </table>
    <?php echo Form::submit('back', '修正', ['class' => 'btn btn_back mh10 mv20']); ?>
    <?php echo Form::submit('registration', '登録', ['class' => 'btn btn_save mh10 mv20']); ?>
<?php echo Form::close(); ?>
</div>
