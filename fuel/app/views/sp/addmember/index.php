<div class="member_profile">
<?php echo Form::open(['action' => '/addmember', 'method' => 'post', 'class' => 'addmember', 'name' => 'addmember']); ?>
    <table class="table table_add">
        <tr class="control_group">
            <th><?php echo Form::label('ログインID', 'login_id'); ?></th>
            <td><?php echo Form::input('login_id', Session::get_flash('login_id'), ['placeholder' => 'メールアドレス']); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('名前'); ?></th>
            <td>
                <?php echo Form::label('姓', 'last_name'); ?>
                <?php echo Form::input('last_name', Session::get_flash('last_name')); ?>
                <?php echo Form::label('名', 'first_name'); ?>
                <?php echo Form::input('first_name', Session::get_flash('first_name')); ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('かな'); ?></th>
            <td>
                <?php echo Form::label('姓', 'last_name_kana'); ?>
                <?php echo Form::input('last_name_kana', Session::get_flash('last_name_kana')); ?>
                <?php echo Form::label('名', 'first_name_kana'); ?>
                <?php echo Form::input('first_name_kana', Session::get_flash('first_name_kana')); ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('年度', 'join_year'); ?></th>
            <td><?php echo Form::select('join_year', Session::get_flash('join_year'), $year_list); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('ポジション', 'position_id'); ?></th>
            <td><?php echo Form::select('position_id', Session::get_flash('position_id'), $positions); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('セクション', 'section_id'); ?></th>
            <td><?php echo Form::select('section_id', Session::get_flash('section_id'), $sections); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('パート', 'part_id'); ?></th>
            <td><?php echo Form::select('part_id', Session::get_flash('part_id'), $parts); ?></td>
        </tr>
    </table>
    <?php echo Form::submit('confirm', '確認', ['class' => 'btn btn_save mv20']); ?>
    <?php echo Form::hidden(Config::get('security.csrf_token_key'), Security::fetch_token());?>
<?php echo Form::close(); ?>
</div>
