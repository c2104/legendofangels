<?php echo Form::open(['action' => '/login', 'method' => 'post', 'class' => 'login mt20']); ?>
    <div class="control_group">
        <?php echo Form::label('ログインID', 'login_id'); ?>
        <?php echo Form::input('login_id', Input::post('login_id')); ?>
    </div>
    <div class="control_group">
        <?php echo Form::label('パスワード', 'password'); ?>
        <?php echo Form::password('password'); ?>
    </div>
    <?php echo Form::submit('login', 'ログイン', ['class' => 'btn btn_login mt20']); ?>
<?php echo Form::close(); ?>
