<div class="change-pass">
<?php echo Form::open(['action' => Config::get('top_url') . '/changepass', 'method' => 'post', 'class' => 'addmember', 'name' => 'addmember']); ?>
    <table class="table table_add">
        <tr class="control_group">
            <th><?php echo Form::label('現在のパスワード', 'old_pass'); ?></th>
            <td><?php echo Form::password('old_pass'); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('新しいパスワード', 'new_pass'); ?></th>
            <td><?php echo Form::password('new_pass'); ?><br>※ 6文字以上の半角英数字( _ - % # )使用可能</td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('新しいパスワード(確認)', 'new_pass_confirm'); ?></th>
            <td><?php echo Form::password('new_pass_confirm'); ?></td>
        </tr>
    </table>
    <?php echo Form::submit('confirm', '確認', ['class' => 'btn btn_save mv20']); ?>
    <?php echo Form::hidden(Config::get('security.csrf_token_key'), Security::fetch_token());?>
<?php echo Form::close(); ?>
</div>
