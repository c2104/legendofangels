<div id="content">
    <div class="whats-new">
        <div class="whats-new__header">
            <table class='table'>
                <thead>
                    <tr><th>新着情報</th><th></th></tr>
                </thead>
            </table>
        </div>
        <div class="whats-new__body">
            <table class='table'>
                <tbody>
                <?php foreach ($infomations as $key => $info): ?>
                    <tr>
                        <td class="whats-new__body--date"><?php echo date('Y年m月d日', strtotime($info['created_at'])) ?></td>
                        <td class="whats-new__body--info"><?php echo nl2br($info['infomation']) ?></td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php // マネージメント ?>
    <?php if ($manage_menu): ?>
        <div class="manage-menu">
            <div class="manage-menu__header">
                <table class='table'>
                    <thead>
                        <tr><th>マネージメント</th></tr>
                    </thead>
                </table>
            </div>
            <div class="manage-menu__body">
                <table class='table'>
                    <tbody>
                        <?php // 安井とつーちゃんと開発者のみ表示 ?>
                        <?php if (in_array($member_id, [26, 79], true) || $developer): ?>
                            <tr class="manage-menu__body--row">
                                <th>
                                    <p>メンバー登録</p>
                                </th>
                            </tr>
                            <tr class="manage-menu__body--row">
                                <td>
                                    <p class="ml20"><a href="/addmember"><span class="txt_hover">ルーキー</span></a></p>
                                </td>
                            </tr>
                            <tr class="manage-menu__body--row">
                                <td>
                                    <p class="ml20"><a href="/addjoinyear"><span class="txt_hover">ベテラン</span></a></p>
                                </td>
                            </tr>
                            <tr class="manage-menu__body--row">
                                <td>
                                    <p><a href="/schedule"><span class="txt_hover">スケジュール編集</span></a></p>
                                </td>
                            </tr>
                        <?php endif ?>
                        <tr class="manage-menu__body--row">
                            <td>
                                <p><a href="/attendcount"><span class="txt_hover">メンバー別出席率</span></a></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif ?>

</div>

<!-- main contents -->