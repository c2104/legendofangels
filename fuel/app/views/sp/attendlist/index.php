<div class="param">
    <?php echo Form::open(['action' => Config::get('top_url').'/attendlist', 'id' => 'fiscal_year', 'method' => 'GET']) ?>
        年度：<?php echo Form::select('year', Input::get('year') ,$year_list, ['class' => 'ph20 pv5', 'onchange' => 'this.form.submit()']) ?>
        <?php if (Input::get('period') == 'all'): ?>
            <button class="param__period" type="get" name="period" value="later">本日以降のみ表示</button>
        <?php else: ?>
            <button class="param__period" type="get" name="period" value="all">全日程を表示</button>
        <?php endif ?>
    <?php echo Form::close() ?>
</div>

<div class="base-frame">
    <div id="schedule-list">
        <?php // 固定ヘッダ ?>
        <div id="corner">
            <table class="table-scroll corner">
                <tr><th>出欠確認</th></tr>
                <tr><td>イベント</td></tr>
                <tr><td>人数</td></tr>
            </table>
        </div>

         <?php // 水平ヘッダ ?>
        <div id="header-h">
            <table class="table-scroll h_fix_tbl">

                <?php //日付 ?>
                <tr>
                    <?php foreach ($schedule_list as $schedule): ?>
                        <th class="day">
                            <?php echo $schedule['month'].'/'.$schedule['day'] ?>
                        </th>
                    <?php endforeach ?>
                </tr>

                <?php //イベント ?>
                <tr>
                    <?php if (count($schedule_list) > 0): ?>
                        <?php $select_column = 1 ?>
                        <?php foreach ($schedule_list as $schedule): ?>
                            <td>
                                <div class="event"><?php echo $schedule['event'] ?></div>
                            </td>
                            <?php $select_column++ ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </tr>

                <?php //人数 ?>
                <tr>
                    <?php if (count($schedule_list) > 0): ?>
                        <?php $select_column = 1 ?>
                        <?php foreach ($schedule_list as $schedule): ?>
                            <td class="attendee_count">
                                <?php foreach ($attendance_list as $attend_id => $attend): ?>
                                    <?php if ($attend_id < 9): ?>
                                        <?php echo mb_substr($attend, 0, 1).': '.$attendee_count[$schedule['id']][$attend_id] ?>
                                        <?php if ($attend_id % 2 == 0): ?>
                                            <br>
                                        <?php endif ?>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </td>
                            <?php $select_column++ ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </tr>
            </table>
        </div>

        <?php // 垂直ヘッダ ?>
        <div id="header-v">
            <table class="table-scroll v-fix-tbl">
                <?php //個人 ?>
                <?php foreach ($member_list as $member): ?>
                    <tr>
                        <td class="v-fix-tbl--name">
                            <?php if ($prof_link): ?>
                                <a href="<?php echo Config::get('top_url') . '/profile?id=' . $member['id'] ?>">
                                    <?php echo $member['last_name'] . ' ' . $member['first_name'] ?></a>
                            <?php else: ?>
                                    <?php echo $member['last_name'] . ' ' . $member['first_name'] ?>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </table>
        </div>

         <!-- データ -->
        <div id="data">
            <table class="table-scroll attend">
                <?php //日別出欠 ?>
                <?php foreach ($member_list as $member): ?>
                    <tr class="personal_row">
                        <?php if (count($schedule_list) > 0): ?>
                            <?php $select_column = 1 ?>
                            <?php foreach ($schedule_list as $schedule): ?>
                                <td class="js-attendance" data-member-id="<?php echo $member['id'] ?>" data-schedule-id="<?php echo $schedule['id'] ?>">
                                    <?php foreach ($member['attendance'] as $attend): ?>
                                        <?php if ($attend['schedule_id'] == $schedule['id']): ?>
                                            <?php echo $attendance_list[$attend['attendance_id']] ?>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </td>
                                <?php $select_column++ ?>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tr>
                <?php endforeach ?>
                <?php //日別出欠 ?>
            </table>
        </div>
    </div>
</div>

<?php echo Asset::js('jquery.fixedTblHdrLftCol-min.js') ?>
