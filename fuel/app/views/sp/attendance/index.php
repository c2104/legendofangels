<?php if (!$is_joined): ?>
    <p><?php echo $current_year ?>年度は参加していません</p>
<?php else: ?>
    <?php echo Form::open(['action' => '/attendance?hash='.Input::get('hash'), 'method' => 'post', 'id' => 'attendance']); ?>
        <div class="attendance-wrapper">
            <div class="attendance-head">
                <div class="attendance-head__title">
                    <h2>
                        <?php echo $current_year ?>年度 出欠確認
                    </h2>
                    <p>
                        <?php echo $name ?>
                    </p>
                </div>
                <div class="attendance-head__submit">
                    <?php echo Form::hidden(Config::get('security.csrf_token_key'), Security::fetch_token());?>
                    <?php echo Form::submit('submit', '出欠登録', ['class' => 'btn btn_login']); ?>
                </div>
            </div>
            <div class="attendance-body">
                <table class="attendance-table table table-hover">
                    <thead>
                        <tr>
                            <th>日付</th>
                            <th>曜日</th>
                            <th>イベント/詳細</th>
                            <th>出欠</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($schedules as $schedule): ?>
                            <?php $attendance_id = Session::get_flash('attendance_'.$schedule['id']) ?>
                            <tr>
                                <td class="attendance-table__date"><?php echo date('n/d', strtotime($schedule['date'])) ?></td>
                                <td class="attendance-table__dow">
                                <?php if (strstr($schedule['dow'], '日') || strstr($schedule['dow'], '祝')): ?>
                                    <span class="cf00"><?php echo $schedule['dow'] ?></span>
                                <?php elseif (strstr($schedule['dow'], '土')): ?>
                                    <span class="c00f"><?php echo $schedule['dow'] ?></span>
                                <?php endif ?>
                                </td>
                                <td class="attendance-table__event"><?php echo $schedule['event'] ?></td>
                                <td class="attendance-table__attendance">
                                    <?php if (Session::get_flash('attendance_'.$schedule['id'])): ?>
                                        <?php echo $attendance[$attendance_id]; ?>
                                        <?php echo Form::hidden('attendance_'.$schedule['id'], Session::get_flash('attendance_'.$schedule['id'])) ?>
                                    <?php else: ?>
                                        <?php echo Form::select('attendance_'.$schedule['id'], Session::get_flash('attendance_'.$schedule['id']), $attendance, ['class' => 'attendance form-control']) ?>
                                    <?php endif ?>
                                </td>
                                <td id="attendance-table__change" class="attendance-table__change" data-toggle="modal" data-target="#attendance-change" data-hash="<?php echo $hash ?>" data-schedule-id="<?php echo $schedule['id'] ?>" data-attend-id="<?php echo $attendance_id ?>">
                                    <?php if (Session::get_flash('attendance_'.$schedule['id'])): ?>
                                        <i class="fa fa-cog fa-2x" aria-hidden="true"></i>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php echo Form::close(); ?>

    <div class="modal modal-chg-attend fade" id="attendance-change" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-chg-attend__header--title">出欠変更</h4>
                </div>
                <div class="modal-body">
                    <p class="modal-chg-attend__body--date"></p>
                    <span class="modal-chg-attend__body--old"></span>
                    <?php echo Form::hidden('old-attendance-id', '') ?>
                    →
                    <?php echo Form::select('new-attendance', '', $attendance, ['class' => 'modal-chg-attend__body--new form-control']) ?>
                    <div class="modal-chg-attend__body--alert alert"></div>
                </div>
                <div class="modal-footer">
                    <button href="/api/attendance/change" type="button" class="modal-chg-attend__footer--close btn btn_close" data-dismiss="modal">閉じる</button>
                    <button href="/api/attendance/change" type="button" class="modal-chg-attend__footer--change btn btn_change">変更</button>
                </div>
            </div>
        </div>
    </div>

<?php endif ?>