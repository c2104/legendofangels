<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow,noarchive" />

    <link rel="icon" href="./favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="./apple-touch-icon-180x180.png" sizes="180x180">

    <?php echo Asset::css('bootstrap.css') ?>
    <?php echo Asset::css('non-responsive.css') ?>

    <?php echo Asset::css('pc/base.css') ?>
    <?php if (File::exists('assets/css/pc/'.$css.'.css')): ?>
        <?php echo Asset::css('pc/'.$css.'.css'); ?>
    <?php endif ?>

    <?php echo Asset::js('jquery-2.2.1.min.js') ?>
    <?php echo Asset::js('bootstrap/collapse.js') ?>
    <?php echo Asset::js('bootstrap/modal.js') ?>
    <?php echo Asset::js('bootstrap/dropdown.js') ?>
    <?php echo Asset::js('common.js') ?>
    <?php echo Asset::js('layout/header.js') ?>
    <?php if (isset($js) && File::exists('assets/js/'.$js.'.js')): ?>
        <?php echo Asset::js($js.'.js') ?>
    <?php endif ?>
    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>

    <title><?php if ($title) {
        echo $title.' | ';
    }
    echo Config::get('band_name');
    ?></title>
</head>
<body>

<?php echo $header; ?>

<div id="container">
    <?php if (Auth::check() && isset($nav)): ?>
        <?php echo $nav; ?>
    <?php endif ?>

    <section>
        <?php if (!empty($title)): ?>
            <h1><?php echo $title ?></h1>
        <?php endif ?>
        <?php if (Session::get_flash('info')): ?>
            <div class="alert alert-info"><?php echo Session::get_flash('info'); ?></div>
        <?php endif ?>
        <?php if (Session::get_flash('error')): ?>
            <div class="alert alert-error"><?php echo Session::get_flash('error'); ?></div>
        <?php endif ?>
        <?php echo $content; ?>
    </section>
    <!-- main contents -->
</div>

<?php echo $footer; ?>

<script type="text/javascript">
$(function(){
    $('#pqp-container').css({
        display: 'block',
        position: 'static'
    }).removeClass('pqp-tallDetails');
    $('#openProfiler').hide();
});
</script>

</body>
</html>