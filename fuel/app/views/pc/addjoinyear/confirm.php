<div class="member_profile">
<?php echo Form::open(['action' => '/addjoinyear/confirm', 'method' => 'post', 'class' => 'addmember', 'name' => 'addjoinyear']); ?>
    <table class="table table_add">
        <tr class="control_group">
            <th><?php echo Form::label('登録メンバー', 'member_id'); ?></th>
            <td>
                <?php echo $name; ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('年度', 'join_year'); ?></th>
            <td>
                <?php echo $join_year; ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('ポジション', 'position'); ?></th>
            <td>
                <?php echo $position_name; ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('セクション', 'section'); ?></th>
            <td>
                <?php echo isset($section_name) ? $section_name : ''; ?>
            </td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('パート', 'part'); ?></th>
            <td>
                <?php echo isset($part_name) ? $part_name : ''; ?>
            </td>
        </tr>
    </table>
    <?php echo Form::submit('back', '修正', ['class' => 'btn btn_back mh10 mv20']); ?>
    <?php echo Form::submit('registration', '登録', ['class' => 'btn btn_save mh10 mv20']); ?>
<?php echo Form::close(); ?>
</div>
