<header>
    <div class="head_area01 navbar navbar-default">
        <div class="navbar-header">
            <?php // バーガーボタン ?>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#gnavi">
                <span class="sr-only">メニュー</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a href="/login" class="band_name navbar-brand"><?php echo Config::get('band_name'); ?></a>
        </div>

        <div id="gnavi" class="collapse navbar-collapse">
            <ul id="menu_top_nav" class="nav navbar-nav navbar-right header-nav">
                <?php if (Auth::check()): ?>
                    <?php if ($member_id == 10): ?>
                        <li class="header-nav__content">
                            <div class="header-nav__content--dev">
                                <button id="header-dropdown-dev" class="dropdown-toggle" type="button" data-toggle="dropdown">
                                    開発<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu header-dropdown" aria-labelledby="header-dropdown-dev">
                                    <li class="header-dropdown__content">
                                        <label><input type="checkbox" class="toggle-on-off header-dropdown__content--toggle-dev" id="header-toggle__dev" data-member-id="<?php echo $member_id ?>" <?php echo $developer ? 'checked' : '' ?>> 管理者権限</label>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    <?php endif ?>
                    <?php if (!Service_Login::isFirstLogin(Auth::get('last_login'))): ?>
                        <li class="header-nav__content">
                            <div class="header-nav__content--config">
                                <button id="header-dropdown-config" class="dropdown-toggle" type="button" data-toggle="dropdown">
                                    設定<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu header-dropdown" aria-labelledby="header-dropdown-config">
                                    <li class="header-dropdown__content">
                                        <a href="/profile">プロフィール編集</a>
                                    </li>
                                    <li class="header-dropdown__content">
                                        <a href="/changepass">パスワード変更</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    <?php endif ?>
                    <li class="header-nav__content"><a href="/logout">ログアウト</a></li>
                <?php else: ?>
                    <li class="header-nav__content"><a href="/login">ログイン</a></li>
                <?php endif ?>
                <li class="header-nav__content"><a href="<?php echo Config::get('official_top_url'); ?>" target="_blank" class="official_link">公式HP <i class="fas fa-external-link-alt fa-fw"></i></a></li>
            </ul>
        </div>
    </div>
</header>