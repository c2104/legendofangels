<nav>
    <div id="sidebar-menu" class="widget sidebar">
        <div class="panel-header">
            メニュー
        </div>
        <?php // 初期パスワードを変更するまで表示しない ?>
        <?php if (!Service_Login::isFirstLogin(Auth::get('last_login'))): ?>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <a href="/memberlist"><span class="txt_hover">メンバー一覧</span></a>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <a href="/attendlist"><span class="txt_hover">出欠一覧</span></a>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </div>
    <?php if ($manage_menu): ?>
        <div id="sidebar-manage" class="widget sidebar">
            <div class="panel-header">
                マネージメント
            </div>
            <?php // 初期パスワードを変更するまで表示しない ?>
            <?php if (!Service_Login::isFirstLogin(Auth::get('last_login'))): ?>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php // 安井とつーちゃんと開発者のみ表示 ?>
                    <?php if (in_array($member_id, [26, 79], true) || $developer): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    メンバー登録 <span class="caret"></span>
                                </a>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <a href="/addmember"><span class="txt_hover">ルーキー</span></a>
                                </div>
                                <div class="panel-body">
                                    <a href="/addjoinyear"><span class="txt_hover">ベテラン</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <a href="/schedule"><span class="txt_hover">スケジュール編集</span></a>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <a href="/attendcount"><span class="txt_hover">メンバー別出席率</span></a>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            <?php endif ?>
        </div>
    <?php endif ?>
</nav>