<div class="schedule-year-list clearfix">
    <div class="w80 pull-left">
        <?php echo Form::open(['action' => '/schedule', 'id' => 'fiscal_year', 'method' => 'GET']) ?>
            年度：<?php echo Form::select('year', Input::get('year') ,$year_list, ['class' => 'ph20 pv5', 'onchange' => 'this.form.submit()']) ?>
        <?php echo Form::close() ?>
    </div>
    <div class="schedule-head__add pull-right">
        <a href="/schedule/add">スケジュール登録</a>
    </div>
</div>
<div class="schedule-wrapper">
    <!--
    <div class="schedule-head">
        <div class="schedule-head__title">
            <h2>
                <?php echo $current_year ?>年度 スケジュール
            </h2>
        </div>
    </div>
     -->
    <div class="schedule-body">
        <table class="schedule-table table table-hover">
            <thead>
                <tr>
                    <th>日付</th>
                    <th>曜日</th>
                    <th>イベント/詳細</th>
                    <th>出欠</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($schedules as $schedule): ?>
                    <tr>
                        <td class="schedule-table__date"><?php echo date('Y/n/d', strtotime($schedule['date'])) ?></td>
                        <td class="schedule-table__dow"><?php echo $schedule['dow'] ?></td>
                        <td class="schedule-table__event"><?php echo $schedule['event'] ?></td>
                        <td class="schedule-table__count-check"><?php echo $schedule['count_check'] ? '＊' : '' ?></td>
                        <td class="schedule-table__edit"><a href="/schedule/edit?id=<?php echo $schedule['id'] ?>">編集</a></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
