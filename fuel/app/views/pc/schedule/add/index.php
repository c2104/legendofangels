<div class="schedule-form">
<?php echo Form::open(['action' => LaUri::baseUrl().'/schedule/add', 'method' => 'post', 'class' => 'addschedule', 'name' => 'addschedule']); ?>
    <table class="table table_add">
        <tr class="control_group">
            <th><?php echo Form::label('年度', 'year'); ?></th>
            <td><?php echo Form::select('year', Session::get_flash('year'), $year_list); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('日程', 'date'); ?></th>
            <td><?php echo Form::input('date', Session::get_flash('date') ?? date('Y-m-d'), ['type' => 'date']); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('出欠カウント', 'count_check'); ?><br><span class="note">※ 出欠に数えない場合はチェックを外してください</span></th>
            <td><?php echo Form::checkbox('count_check', 1, Session::get_flash('count_check')); ?></td>
        </tr>
        <tr class="control_group">
            <th><?php echo Form::label('詳細', 'event'); ?></th>
            <td><?php echo Form::input('event', Session::get_flash('event')); ?></td>
        </tr>
    </table>
    <?php echo Form::submit('confirm', '確認', ['class' => 'btn btn_save mv20']); ?>
    <?php echo Form::hidden(Config::get('security.csrf_token_key'), Security::fetch_token());?>
<?php echo Form::close(); ?>
</div>
