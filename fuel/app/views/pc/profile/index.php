<div class="contents">
    <table class="member_profile">
        <tbody>
            <tr class="control_group">
                <th>ログインID (メールアドレス)</th>
                <?php if ($profile['login_id']): ?>
                    <td>
                    <div class="login_id_set">
                        <div>
                            <p class="login_id"><?php echo $profile['login_id'] ?></p>
                            <input id="form_login_id" type="text" value="">
                        </div>
                    </div>
                    </td>
                <?php endif ?>
            </tr>
            <tr class="control_group">
                <th>名前</th>
                <td>
                    <span class="name mr10">
                        <?php echo $profile['last_name'] ?>
                        <?php echo $profile['first_name'] ?>
                    </span>
                    <span class="secondary_text fz11">
                        <?php echo $profile['last_name_kana'] ?>
                        <?php echo $profile['first_name_kana'] ?>
                    </span>
                </td>
            </tr>
            <?php if ($profile['id'] == $member_id || $developer): ?>
                <tr class="control_group">
                    <th>出欠URL</th>
                    <td>
                        <span class="mr10">
                            <?php echo Form::input('attendance_url', Config::get('top_url').'/attendance?hash='.$profile['hash'], ['onClick' => 'this.select(0,this.value.length)', 'readonly']) ?>
                            <a href="/attendance?hash=<?php echo $profile['hash'] ?>" target="_blank">ページヘ</a>
                        </span>
                    </td>
                </tr>
            <?php endif ?>
            <tr class="control_group">
                <th>参加年度</th>
                <td>
                    <p class="ta-r">
                        <span class="join_count">参加年数: <?php echo $join_count; ?>年</span>
                        / サポート年数: <?php echo $support_count; ?>年
                    </p>
                    <!-- 参加年度 -->
                    <table class="join_year_list">
                            <thead>
                            <tr>
                                <th>年度</th>
                                <th>ポジション</th>
                                <th>セクション</th>
                                <th>パート</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($join_year_list as $join_year): ?>
                                <tr>
                                    <td><?php echo $join_year['join_year']; ?></td>
                                    <td>
                                        <?php foreach ($positions as $position): ?>
                                            <?php if (in_array($position['id'], $join_year['position_id'])): ?>
                                                <?php echo $position['name']; ?><br>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </td>
                                    <td>
                                        <?php foreach ($sections as $section): ?>
                                            <?php if ($join_year['section_id'] == $section['id']): ?>
                                                <?php echo $section['name']; ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </td>
                                    <td>
                                        <?php foreach ($parts as $part): ?>
                                            <?php if ($join_year['part_id'] == $part['id']): ?>
                                                <?php echo $part['name']; ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <!-- /参加年度 -->
                </td>
            </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript">
$(function() {
    $('.change_mail').on('click', function() {
        login_id = $('.login_id').html();
        $('.login_id').css('display', 'none');
        $('#form_login_id').val(login_id).css('display', 'block')
        $(this).css('display', 'none');
        $('.save_mail').css('display', 'block')
    });
    $('.save_mail').on('click', function() {
        login_id = $('#form_login_id').val();
        $('.login_id').css('display', 'block').html(login_id);
        $('#form_login_id').val(login_id).css('display', 'none')
        $(this).css('display', 'none');
        $('.change_mail').css('display', 'block')
    });
});
</script>