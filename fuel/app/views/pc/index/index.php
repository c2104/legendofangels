<div id="content">
    <div class="whats-new">
        <div class="whats-new__header">
            <table class='table'>
                <thead>
                    <tr><th>新着情報</th><th></th></tr>
                </thead>
            </table>
        </div>
        <div class="whats-new__body">
            <table class='table'>
                <tbody>
                <?php foreach ($infomations as $key => $info): ?>
                    <tr>
                        <td class="whats-new__body--date"><?php echo date('Y年m月d日', strtotime($info['created_at'])) ?></td>
                        <td class="whats-new__body--info"><?php echo nl2br($info['infomation']) ?></td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- main contents -->