<div class="ta-l">
    <div class="fl-l ph20">
        <h2>Toggle</h2>
        <div>
            <input type="checkbox" class="toggle-on-off" id="toggle-hoge" checked>
            <label for="toggle-hoge" class="toggle-on-off--label" data-on-label="on" data-off-label="off"></label>
        </div>
    </div>
    <div>
        <pre>
        <code>
            &lt;div&gt;
                &lt;input type="checkbox" class="toggle-on-off" id="toggle-hoge" checked&gt;
                &lt;label for="toggle-hoge" data-on-label="on" data-off-label="off"&gt;&lt;/label&gt;
            &lt;/div&gt;
        </code>
        </pre>
    </div>
</div>
<!-- main contents -->