<div class="member_profile">
    <h2 class="mv10">登録が完了しました。</h2>
    <p class="member-profile__done--msg"><?php echo nl2br($msg) ?></p>
    <a href="/"><?php echo Form::button('addmember', 'トップへ', ['class' => 'btn btn_top mh10 mv20']); ?></a>
    <a href="/addmember"><?php echo Form::button('addmember', '続けて登録', ['class' => 'btn btn_next mh10 mv20']); ?></a>
</div>
