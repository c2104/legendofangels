<div class="year_list">
    <?php echo Form::open(['action' => '/attendcount', 'id' => 'fiscal_year', 'method' => 'GET']) ?>
        <p class="mb10 ta-l">年度：<?php echo Form::select('year', Input::get('year'), $year_list, ['class' => 'ph20 pv5', 'onchange' => 'this.form.submit()']) ?></p>
    <?php echo Form::close() ?>
</div>
<div class="member-list">
    <div class="position">
        <table class="table member-list__table">
            <caption class="position_name">
                出欠を数える日程数<span class="m10 fz25"><?php echo $all_schedule_count; ?></span>日
            </caption>
            <tr class="member-list__header">
                <?php if ($developer): ?>
                    <th class="member-list__header--id">id</th>
                <?php endif ?>
                <th class="member-list__header--name">名前</th>
                <th class="member-list__header--attendance-count">出欠数</th>
                <th class="member-list__header--attendance-rate">出席率</th>
                <th class="member-list__header--perfect-attendance">皆勤</th>
            </tr>
            <?php foreach ($attendance_rate_list as $member_id => $attendance_rate): ?>
                <tr class="member-list__body">
                    <?php if ($developer): ?>
                        <td class="member-list__body--id"><?php echo $member_id ?></td>
                    <?php endif ?>
                    <td class="member-list__body--name">
                        <?php if ($prof_link): ?>
                            <a href="<?php echo '/profile?id='.$member_id ?>"><?php echo $member_list[$member_id]['last_name'].' '.$member_list[$member_id]['first_name'] ?></a>
                        <?php else: ?>
                            <?php echo $member_list[$member_id]['last_name'].' '.$member_list[$member_id]['first_name'] ?>
                        <?php endif ?>
                        <span class="secondary_text fz11"><?php echo $member_list[$member_id]['last_name_kana'].' '.$member_list[$member_id]['first_name_kana'] ?></span>
                    </td>
                    <td class="member-list__body--attendance-count">
                        <?php if (isset($attendance_count_list[$member_id])): ?>
                            <?php $member_attendance = $attendance_count_list[$member_id]; ?>
                            <p class="mb5 <?php echo ($all_schedule_count != $member_attendance['total']) ? 'cf00 bold' : '' ?>">出欠登録数 <?php echo $member_attendance['total'] ?></p>
                            <p>出席：<?php echo isset($member_attendance[1]) ? $member_attendance[1]['attendance_count'] : 0; ?></p>
                            <p>欠席：<?php echo isset($member_attendance[2]) ? $member_attendance[2]['attendance_count'] : 0; ?></p>
                            <p>遅刻：<?php echo isset($member_attendance[3]) ? $member_attendance[3]['attendance_count'] : 0; ?></p>
                            <p>早退：<?php echo isset($member_attendance[4]) ? $member_attendance[4]['attendance_count'] : 0; ?></p>
                            <p>未定：<?php echo isset($member_attendance[9]) ? $member_attendance[9]['attendance_count'] : 0; ?></p>
                            <p>-：<?php echo isset($member_attendance[10]) ? $member_attendance[10]['attendance_count'] : 0; ?></p>
                        <?php endif ?>
                    </td>
                    <td class="member-list__body--attendance-rate">
                        <p>「出席」のみ<span class="m10 fz25"><?php echo $attendance_rate['only_attend'] ?></span>%</p>
                        <p> 「出席 遅刻 早退」を含む<span class="m10"><?php echo $attendance_rate['full_attend'] ?></span>%</p>
                    </td>
                    <td class="member-list__body--perfect-attendance">
                        <p><?php echo $attendance_rate['perfect_attendance'] ? '<i class="far fa-star fa-2x m10 gold"></i>' : '' ?></p>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
</div>
