<div class="year_list">
    <?php echo Form::open(['action' => '/memberlist', 'id' => 'fiscal_year', 'method' => 'GET']) ?>
        <p class="mb10 ta-l">年度：<?php echo Form::select('year', Input::get('year'), $year_list, ['class' => 'ph20 pv5', 'onchange' => 'this.form.submit()']) ?></p>
    <?php echo Form::close() ?>
</div>
<div class="member-list">
    <?php foreach ($positions as $position): ?>
        <div class="position">
            <table class="table member-list__table">
                <caption class="position_name">
                    <?php echo $position['name'] ?>
                    <?php echo isset($position_count[$position['id']]) ? $position_count[$position['id']] : 0 ?>名
                </caption>
                <tr class="member-list__header">
                    <?php if ($developer): ?>
                        <th class="member-list__header--id">id</th>
                    <?php endif ?>
                    <th class="member-list__header--name">名前</th>
                    <th class="member-list__header--section">セクション</th>
                    <th class="member-list__header--part">パート</th>
                </tr>
                <?php foreach ($member_list as $member): ?>
                    <?php if (in_array($position['id'], $member['position_id'])): ?>
                        <tr class="member-list__body">
                            <?php if ($developer): ?>
                                <td class="member-list__body--id"><?php echo $member['id'] ?></td>
                            <?php endif ?>
                            <td class="member-list__body--name">
                                <?php if ($prof_link): ?>
                                    <a href="<?php echo LaUri::baseUrl().'/profile?id='.$member['id'] ?>"><?php echo $member['last_name'].' '.$member['first_name'] ?></a>
                                <?php else: ?>
                                    <?php echo $member['last_name'].' '.$member['first_name'] ?>
                                <?php endif ?>
                                <span class="secondary_text fz11"><?php echo $member['last_name_kana'].' '.$member['first_name_kana'] ?></span>
                            </td>
                            <td class="member-list__body--section">
                                <?php foreach ($sections as $section): ?>
                                    <?php if ($member['section_id'] == $section['id']): ?>
                                        <?php echo $section['name'] ?>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </td>
                            <td class="member-list__body--part">
                                <?php foreach ($parts as $part): ?>
                                    <?php if ($member['part_id'] == $part['id']): ?>
                                        <?php echo $part['name'] ?>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </td>
                        </tr>
                    <?php endif ?>
                <?php endforeach ?>
            </table>
        </div>
    <?php endforeach ?>
</div>
