<?php
/**
 * Fuel
 *
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.7
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2015 Fuel Development Team
 * @link       http://fuelphp.com
 */

namespace Auth;

class MemberUserUpdateException extends \FuelException {}

class MemberUserWrongPassword extends \FuelException {}

/**
 * Member basic login driver
 *
 * @package     Fuel
 * @subpackage  Auth
 */
class Auth_Login_MemberAuth extends \Auth_Login_Driver
{
	/**
	 * Load the config and setup the remember-me session if needed
	 */
	public static function _init()
	{
		\Config::load('memberauth', true, true, true);
	}

	/**
	 * @var  Database_Result  when login succeeded
	 */
	protected $user = null;

	/**
	 * @var  array  value for guest login
	 */
	protected static $guest_login = array(
		'id' => 0,
		'login_id' => 'guest',
		'group' => '0',
		'login_hash' => false,
		'email' => false,
	);

	/**
	 * @var  array  MemberAuth class config
	 */
	protected $config = array(
		'drivers' => array('group' => array('Membergroup')),
		'additional_fields' => array('profile_fields'),
	);

	/**
	 * Check for login
	 *
	 * @return  bool
	 */
	protected function perform_check()
	{
		// fetch the login_id and login hash from the session
		$login_id    = \Session::get('login_id');
		$login_hash  = \Session::get('login_hash');

		// only worth checking if there's both a login_id and login-hash
		if ( ! empty($login_id) and ! empty($login_hash))
		{
			if (is_null($this->user) or ($this->user['login_id'] != $login_id and $this->user != static::$guest_login))
			{
				$this->user = \DB::select_array(\Config::get('memberauth.table_columns', array('*')))
					->where('login_id', '=', $login_id)
					->from(\Config::get('memberauth.table_name'))
					->execute(\Config::get('memberauth.db_connection'))->current();
			}
			if(is_null($this->user)){
				return false;
			}
			// return true when login was verified, and either the hash matches or multiple logins are allowed
			// if ($this->user and (\Config::get('memberauth.multiple_logins', false) or $this->user['login_hash'] === $login_hash))
			if ($this->user)
			{
				return true;
			}
		}

		// no valid login when still here, ensure empty session and optionally set guest_login
		$this->user = \Config::get('memberauth.guest_login', true) ? static::$guest_login : false;
		\Session::delete('login_id');
		\Session::delete('login_hash');

		return false;
	}

	/**
	 * Check the user exists
	 *
	 * @return  bool
	 */
	public function validate_user($login_id = '', $password = '')
	{
		$login_id = trim($login_id) ?: trim(\Input::post(\Config::get('memberauth.login_id_post_key', 'login_id')));
		$password = trim($password) ?: trim(\Input::post(\Config::get('memberauth.password_post_key', 'password')));

		if (empty($login_id) or empty($password))
		{
			return false;
		}

		$password = $this->hash_password($password);
		$user = \DB::select_array(\Config::get('memberauth.table_columns', array('*')))
			->where_open()
			->where('login_id', '=', $login_id)
			->where_close()
			->where('password', '=', $password)
			->from(\Config::get('memberauth.table_name'))
			->execute(\Config::get('memberauth.db_connection'))->current();

		return $user ?: false;
	}

	/**
	 * Login user
	 *
	 * @param   string
	 * @param   string
	 * @return  bool
	 */
	public function login($login_id = '', $password = '')
	{
		if ( ! ($this->user = $this->validate_user($login_id, $password)))
		{
			$this->user = \Config::get('memberauth.guest_login', true) ? static::$guest_login : false;
			\Session::delete('login_id');
			\Session::delete('login_hash');
			return false;
		}

		// register so Auth::logout() can find us
		Auth::_register_verified($this);

		\Session::set('login_id', $this->user['login_id']);
		\Session::set('login_hash', $this->create_login_hash());
		\Session::instance()->rotate();
		return true;
	}

	/**
	 * Force login user
	 *
	 * @param   string
	 * @return  bool
	 */
	public function force_login($user_id = '')
	{
		if (empty($user_id))
		{
			return false;
		}

		$this->user = \DB::select_array(\Config::get('memberauth.table_columns', array('*')))
			->where_open()
			->where('id', '=', $user_id)
			->where_close()
			->from(\Config::get('memberauth.table_name'))
			->execute(\Config::get('memberauth.db_connection'))
			->current();

		if ($this->user == false)
		{
			$this->user = \Config::get('memberauth.guest_login', true) ? static::$guest_login : false;
			\Session::delete('login_id');
			\Session::delete('login_hash');
			return false;
		}

		\Session::set('login_id', $this->user['login_id']);
		\Session::set('login_hash', $this->create_login_hash());

		// and rotate the session id, we've elevated rights
		\Session::instance()->rotate();

		// register so Auth::logout() can find us
		Auth::_register_verified($this);

		return true;
	}

	/**
	 * Logout user
	 *
	 * @return  bool
	 */
	public function logout()
	{
		$this->user = \Config::get('memberauth.guest_login', true) ? static::$guest_login : false;
		\Session::delete('login_id');
		\Session::delete('login_hash');
		return true;
	}

	/**
	 * Create new user
	 *
	 * @param   string
	 * @param   string
	 * @param   string  must contain valid email address
	 * @param   int     group id
	 * @param   Array
	 * @return  bool
	 */
	public function create_user($login_id, $password, $member_info)
	{
		$password = trim($password);
		$login_id = filter_var(trim($login_id), FILTER_VALIDATE_EMAIL);

		if (empty($login_id))
		{
			throw new \MemberUserUpdateException('ログインIDを入力してください。', 1);
		}

		$same_users = \DB::select_array(\Config::get('memberauth.table_columns', array('*')))
			->where('login_id', '=', $login_id)
			->from(\Config::get('memberauth.table_name'))
			->execute(\Config::get('memberauth.db_connection'));

		if ($same_users->count() > 0)
		{
			throw new \MemberUserUpdateException('そのメールアドレスはすでに使用されています。', 3);
		}

		// 固有ハッシュを生成
		$members = \DB::select()->from('member')->execute()->as_array();
		foreach ($members as $member) {
			$hash = \Str::random('alnum', 40);
			$member_hash = \DB::select('hash')->from('member')->execute()->as_array();
			do {
				if (in_array($hash, $member_hash)) {
					$hash = \Str::random('alnum', 40);
				} else {
					break;
				}
			} while (0);
		}

		$user = array(
			'login_id'        => (string) $login_id,
			'password'        => $this->hash_password((string) $password),
			'last_name'       => $member_info['last_name'],
			'first_name'      => $member_info['first_name'],
			'last_name_kana'  => $member_info['last_name_kana'],
			'first_name_kana' => $member_info['first_name_kana'],
			'hash'            => $hash,
			'login_hash'      => '',
			'last_login'      => '0000-00-00 00:00:00',
			'created_at'      => \Date::time()->format('mysql')
		);
		$result = \DB::insert(\Config::get('memberauth.table_name'))
			->set($user)
			->execute(\Config::get('memberauth.db_connection'));

		return ($result[1] > 0) ? $result[0] : false;
	}

	/**
	 * Update a user's properties
	 * Note: login_id cannot be updated, to update password the old password must be passed as old_password
	 *
	 * @param   Array  properties to be updated including profile fields
	 * @param   string
	 * @return  bool
	 */
	public function update_user($values, $login_id = null)
	{
		$login_id = $login_id ?: $this->user['login_id'];
		$current_values = \DB::select_array(\Config::get('memberauth.table_columns', array('*')))
			->where('login_id', '=', $login_id)
			->from(\Config::get('memberauth.table_name'))
			->execute(\Config::get('memberauth.db_connection'));

		if (empty($current_values))
		{
			throw new \MemberUserUpdateException('login_id not found', 4);
		}

		$update = array();
		if (array_key_exists('login_id', $values))
		{
			throw new \MemberUserUpdateException('login_id cannot be changed.', 5);
		}
		if (array_key_exists('password', $values))
		{
			if (empty($values['old_password'])
				or $current_values->get('password') != $this->hash_password(trim($values['old_password'])))
			{
				throw new \SimpleUserWrongPassword('Old password is invalid');
			}

			$password = trim(strval($values['password']));
			if ($password === '')
			{
				throw new \MemberUserUpdateException('Password can\'t be empty.', 6);
			}
			$update['password'] = $this->hash_password($password);
			unset($values['password']);
		}
		if (array_key_exists('old_password', $values))
		{
			unset($values['old_password']);
		}
		if (array_key_exists('email', $values))
		{
			$email = filter_var(trim($values['email']), FILTER_VALIDATE_EMAIL);
			if ( ! $email)
			{
				throw new \MemberUserUpdateException('Email address is not valid', 7);
			}
			$matches = \DB::select()
				->where('email', '=', $email)
				->where('id', '!=', $current_values[0]['id'])
				->from(\Config::get('memberauth.table_name'))
				->execute(\Config::get('memberauth.db_connection'));
			if (count($matches))
			{
				throw new \MemberUserUpdateException('Email address is already in use', 11);
			}
			$update['email'] = $email;
			unset($values['email']);
		}
		if (array_key_exists('group', $values))
		{
			if (is_numeric($values['group']))
			{
				$update['group'] = (int) $values['group'];
			}
			unset($values['group']);
		}
		if ( ! empty($values))
		{
			$profile_fields = @unserialize($current_values->get('profile_fields')) ?: array();
			foreach ($values as $key => $val)
			{
				if ($val === null)
				{
					unset($profile_fields[$key]);
				}
				else
				{
					$profile_fields[$key] = $val;
				}
			}
			$update['profile_fields'] = serialize($profile_fields);
		}

		// $update['updated_at'] = \Date::forge()->get_timestamp();
		$update['updated_at'] = date('Y-m-d H:i:s');

		$affected_rows = \DB::update(\Config::get('memberauth.table_name'))
			->set($update)
			->where('login_id', '=', $login_id)
			->execute(\Config::get('memberauth.db_connection'));

		// Refresh user
		if ($this->user['login_id'] == $login_id)
		{
			$this->user = \DB::select_array(\Config::get('memberauth.table_columns', array('*')))
				->where('login_id', '=', $login_id)
				->from(\Config::get('memberauth.table_name'))
				->execute(\Config::get('memberauth.db_connection'))->current();
		}

		return $affected_rows > 0;
	}

	/**
	 * Change a user's password
	 *
	 * @param   string
	 * @param   string
	 * @param   string  login_id or null for current user
	 * @return  bool
	 */
	public function change_password($old_password, $new_password, $login_id = null)
	{
		try
		{
			return (bool) $this->update_user(array('old_password' => $old_password, 'password' => $new_password), $login_id);
		}
		// Only catch the wrong password exception
		catch (SimpleUserWrongPassword $e)
		{
			return false;
		}
	}

	/**
	 * Generates new random password, sets it for the given login_id and returns the new password.
	 * To be used for resetting a user's forgotten password, should be emailed afterwards.
	 *
	 * @param   string  $login_id
	 * @return  string
	 */
	public function reset_password($login_id)
	{
		$new_password = \Str::random('alnum', 8);
		$password_hash = $this->hash_password($new_password);

		$affected_rows = \DB::update(\Config::get('memberauth.table_name'))
			->set(array('password' => $password_hash))
			->where('login_id', '=', $login_id)
			->execute(\Config::get('memberauth.db_connection'));

		if ( ! $affected_rows)
		{
			throw new \MemberUserUpdateException('Failed to reset password, user was invalid.', 8);
		}

		return $new_password;
	}

	/**
	 * Deletes a given user
	 *
	 * @param   string
	 * @return  bool
	 */
	public function delete_user($login_id)
	{
		if (empty($login_id))
		{
			throw new \MemberUserUpdateException('Cannot delete user with empty login_id', 9);
		}

		$affected_rows = \DB::delete(\Config::get('memberauth.table_name'))
			->where('login_id', '=', $login_id)
			->execute(\Config::get('memberauth.db_connection'));

		return $affected_rows > 0;
	}

	/**
	 * Creates a temporary hash that will validate the current login
	 *
	 * @return  string
	 */
	public function create_login_hash()
	{
		if (empty($this->user))
		{
			throw new \MemberUserUpdateException('User not logged in, can\'t create login hash.', 10);
		}

		$last_login = \Date::forge()->get_timestamp();
		$login_hash = sha1(\Config::get('memberauth.login_hash_salt').$this->user['login_id'].$last_login);

		\DB::update(\Config::get('memberauth.table_name'))
			->set(array('last_login' => date('Y-m-d H:i:s', $last_login), 'login_hash' => $login_hash))
			->where('login_id', '=', $this->user['login_id'])
			->execute(\Config::get('memberauth.db_connection'));

		$this->user['login_hash'] = $login_hash;

		return $login_hash;
	}

	/**
	 * Get the user's ID
	 *
	 * @return  Array  containing this driver's ID & the user's ID
	 */
	public function get_user_id()
	{
		if (empty($this->user))
		{
			return false;
		}

		return array($this->id, (int) $this->user['id']);
	}

	/**
	 * Get the user's groups
	 *
	 * @return  Array  containing the group driver ID & the user's group ID
	 */
	public function get_groups()
	{
		if (empty($this->user))
		{
			return false;
		}

		return array(array('Simplegroup', $this->user['group']));
	}

	/**
	 * Getter for user data
	 *
	 * @param  string  name of the user field to return
	 * @param  mixed  value to return if the field requested does not exist
	 *
	 * @return  mixed
	 */
	public function get($field, $default = null)
	{
		if (isset($this->user[$field]))
		{
			return $this->user[$field];
		}
		elseif (isset($this->user['profile_fields']))
		{
			return $this->get_profile_fields($field, $default);
		}

		return $default;
	}

	/**
	 * Get the user's emailaddress
	 *
	 * @return  string
	 */
	public function get_email()
	{
		return $this->get('email', false);
	}

	/**
	 * Get the user's screen name
	 *
	 * @return  string
	 */
	public function get_screen_name()
	{
		if (empty($this->user))
		{
			return false;
		}

		return $this->user['login_id'];
	}

	/**
	 * Get the user's profile fields
	 *
	 * @return  Array
	 */
	public function get_profile_fields($field = null, $default = null)
	{
		if (empty($this->user))
		{
			return false;
		}

		if (isset($this->user['profile_fields']))
		{
			is_array($this->user['profile_fields']) or $this->user['profile_fields'] = (@unserialize($this->user['profile_fields']) ?: array());
		}
		else
		{
			$this->user['profile_fields'] = array();
		}

		return is_null($field) ? $this->user['profile_fields'] : \Arr::get($this->user['profile_fields'], $field, $default);
	}

	/**
	 * Extension of base driver method to default to user group instead of user id
	 */
	public function has_access($condition, $driver = null, $user = null)
	{
		if (is_null($user))
		{
			$groups = $this->get_groups();
			$user = reset($groups);
		}
		return parent::has_access($condition, $driver, $user);
	}

	/**
	 * Extension of base driver because this supports a guest login when switched on
	 */
	public function guest_login()
	{
		return \Config::get('memberauth.guest_login', true);
	}

    public function get_hash_password(string $password)
    {
        return $this->hash_password((string) $password);
    }
}

// end of file memberauth.php
